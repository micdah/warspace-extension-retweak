
1. Download Inno Setup.
http://www.jrsoftware.org/isinfo.php

2. Modify the .iss file.

3. Modify the other files to your liking.

4. Run the .iss file, it will open the Inno Setup Compiler. Go to the tab Build > Compile. The setup EXE will be created in the folder "Output".

5. Done. But you probably want to test everything to make sure everything works.



Notes:

The .iss file is the actual code for the setup program. All the other files are copied or run by the .iss code.
For this installer I made it so some files are manipulated by .bat files because there are some limitations to the setup program.

The official documentation provides a lot of explanations for pretty much everything.
http://www.jrsoftware.org/ishelp/

Go to "Setup Scripts Sections". It explains all the different sections and what you can put in them.