
REM This restores files. Do not run manually! Use the uninstaller to uninstall the mod or things might get screwed up.


REM Restoring common files.

copy "(WE Backup) XComGame.exe" "..\Binaries\Win32\XComGame.exe"
copy "(WE Backup) XComGame.upk" "..\XComGame\CookedPCConsole\XComGame.upk"
copy "(WE Backup) XComStrategyGame.upk" "..\XComGame\CookedPCConsole\XComStrategyGame.upk"
copy "(WE Backup) XComGame.upk.uncompressed_size" "..\XComGame\CookedPCConsole\XComGame.upk.uncompressed_size"
copy "(WE Backup) XComStrategyGame.upk.uncompressed_size" "..\XComGame\CookedPCConsole\XComStrategyGame.upk.uncompressed_size"



REM Restoring Slingshot files.

del "..\XComGame\DLC\PCConsole\DLC_Day060\Config\XComGame.ini"
copy "(WE Backup) XComGame.ini" "..\XComGame\DLC\PCConsole\DLC_Day060\Config\XComGame.ini"
del "..\XComGame\DLC\PCConsole\DLC_Day060\CookedPCConsole\DLC_Day060_SF.upk"
copy "(WE Backup) DLC_Day060_SF.upk" "..\XComGame\DLC\PCConsole\DLC_Day060\CookedPCConsole\DLC_Day060_SF.upk"



REM Restoring localization files.
REM The following scripts move stuff to the My Docs folder. Should work for all users because it fetches the data from the Registry.

REM This one is for users with old Windows. Note the single TAB as a delim.
FOR /F "tokens=3 delims=	" %%G IN ('REG QUERY "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" /v "Personal"') DO (SET docsdir=%%G)
REM English files.
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComGame.int"
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComStrategyGame.int"
copy "(WE Backup Game Folder) XComGame.int" "..\XComGame\Localization\INT\XComGame.int"
copy "(WE Backup My Docs) XComGame.int" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComGame.int"
copy "(WE Backup My Docs) XComStrategyGame.int" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComStrategyGame.int"
REM French files.
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComGame.fra"
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComStrategyGame.fra"
copy "(WE Backup My Docs) XComGame.fra" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComGame.fra"
copy "(WE Backup My Docs) XComStrategyGame.fra" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComStrategyGame.fra"
REM Spanish files.
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComGame.esn"
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComStrategyGame.esn"
copy "(WE Backup My Docs) XComGame.esn" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComGame.esn"
copy "(WE Backup My Docs) XComStrategyGame.esn" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComStrategyGame.esn"


REM The following is for users with new Windows. Note the SPACE as a delim (default action in new Windows).
REM For users with no space in the actual path.
FOR /F "tokens=3*" %%G IN ('REG QUERY "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" /v "Personal"') DO (SET docsdir=%%G)
REM English files.
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComGame.int"
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComStrategyGame.int"
copy "(WE Backup Game Folder) XComGame.int" "..\XComGame\Localization\INT\XComGame.int"
copy "(WE Backup My Docs) XComGame.int" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComGame.int"
copy "(WE Backup My Docs) XComStrategyGame.int" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComStrategyGame.int"
REM French files.
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComGame.fra"
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComStrategyGame.fra"
copy "(WE Backup My Docs) XComGame.fra" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComGame.fra"
copy "(WE Backup My Docs) XComStrategyGame.fra" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComStrategyGame.fra"
REM Spanish files.
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComGame.esn"
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComStrategyGame.esn"
copy "(WE Backup My Docs) XComGame.esn" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComGame.esn"
copy "(WE Backup My Docs) XComStrategyGame.esn" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComStrategyGame.esn"


REM For users with 1 space in the actual path.
FOR /F "tokens=3*" %%G IN ('REG QUERY "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" /v "Personal"') DO (SET docsdir=%%G %%H)
REM English files.
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComGame.int"
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComStrategyGame.int"
copy "(WE Backup Game Folder) XComGame.int" "..\XComGame\Localization\INT\XComGame.int"
copy "(WE Backup My Docs) XComGame.int" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComGame.int"
copy "(WE Backup My Docs) XComStrategyGame.int" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComStrategyGame.int"
REM French files.
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComGame.fra"
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComStrategyGame.fra"
copy "(WE Backup My Docs) XComGame.fra" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComGame.fra"
copy "(WE Backup My Docs) XComStrategyGame.fra" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComStrategyGame.fra"
REM Spanish files.
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComGame.esn"
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComStrategyGame.esn"
copy "(WE Backup My Docs) XComGame.esn" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComGame.esn"
copy "(WE Backup My Docs) XComStrategyGame.esn" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComStrategyGame.esn"


REM For users with 2 spaces in the actual path.
FOR /F "tokens=3*" %%G IN ('REG QUERY "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" /v "Personal"') DO (SET docsdir=%%G %%H %%I)
REM English files.
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComGame.int"
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComStrategyGame.int"
copy "(WE Backup Game Folder) XComGame.int" "..\XComGame\Localization\INT\XComGame.int"
copy "(WE Backup My Docs) XComGame.int" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComGame.int"
copy "(WE Backup My Docs) XComStrategyGame.int" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComStrategyGame.int"
REM French files.
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComGame.fra"
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComStrategyGame.fra"
copy "(WE Backup My Docs) XComGame.fra" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComGame.fra"
copy "(WE Backup My Docs) XComStrategyGame.fra" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComStrategyGame.fra"
REM Spanish files.
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComGame.esn"
del "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComStrategyGame.esn"
copy "(WE Backup My Docs) XComGame.esn" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComGame.esn"
copy "(WE Backup My Docs) XComStrategyGame.esn" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComStrategyGame.esn"




REM The following will restore old localization backups. This is for users who already had the mod installed before the installer got overhauled.
REM For users with Warspace v1.53 and older.

copy "(WE Backup) XComGame.int" "..\XComGame\Localization\INT\XComGame.int"

REM Moving stuff to the My Docs folder. Should work for all users because it fetches the data from the Registry.
REM For users with old Windows. Note the single TAB as a delim.
FOR /F "tokens=3 delims=	" %%G IN ('REG QUERY "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" /v "Personal"') DO (SET docsdir=%%G)
copy "(WE Backup) XComStrategyGame.INT" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComStrategyGame.INT"
copy "(WE Backup) MyDocs XComGame.INT" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComGame.INT"


REM For users with new Windows. Note the SPACE as a delim (default action in new Windows).
REM For users with no space in the actual path.
FOR /F "tokens=3*" %%G IN ('REG QUERY "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" /v "Personal"') DO (SET docsdir=%%G)
copy "(WE Backup) XComStrategyGame.INT" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComStrategyGame.INT"
copy "(WE Backup) MyDocs XComGame.INT" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComGame.INT"


REM For users with 1 space in the actual path.
FOR /F "tokens=3*" %%G IN ('REG QUERY "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" /v "Personal"') DO (SET docsdir=%%G %%H)
copy "(WE Backup) XComStrategyGame.INT" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComStrategyGame.INT"
copy "(WE Backup) MyDocs XComGame.INT" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComGame.INT"


REM For users with 2 spaces in the actual path.
FOR /F "tokens=3*" %%G IN ('REG QUERY "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" /v "Personal"') DO (SET docsdir=%%G %%H %%I)
copy "(WE Backup) XComStrategyGame.INT" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComStrategyGame.INT"
copy "(WE Backup) MyDocs XComGame.INT" "%docsdir%\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComGame.INT"
