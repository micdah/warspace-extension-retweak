
[XComGame.XGTacticalGameCore]
														  
Armors=(ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,Properties[0]=eAP_None,Properties[1]=eAP_None,Properties[2]=eAP_None,Properties[3]=eAP_None,iType=eItem_ArmorKevlar,iHPBonus=5,iDefenseBonus=0,iFlightFuel=0,iWillBonus=0,iLargeItems=1,iSmallItems=1,iMobilityBonus=0)
Armors=(ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,Properties[0]=eAP_None,Properties[1]=eAP_None,Properties[2]=eAP_None,Properties[3]=eAP_None,iType=eItem_ArmorCarapace,iHPBonus=7,iDefenseBonus=0,iFlightFuel=0,iWillBonus=10,iLargeItems=1,iSmallItems=2,iMobilityBonus=-1)
Armors=(ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,Properties[0]=eAP_Grapple,Properties[1]=eAP_None,Properties[2]=eAP_None,Properties[3]=eAP_None,iType=eItem_ArmorSkeleton,iHPBonus=6,iDefenseBonus=8,iFlightFuel=0,iWillBonus=5,iLargeItems=1,iSmallItems=1,iMobilityBonus=2)
Armors=(ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,Properties[0]=eAP_DualWield,Properties[1]=eAP_PoisonImmunity,Properties[2]=eAP_None,Properties[3]=eAP_None,iType=eItem_ArmorTitan,iHPBonus=10,iDefenseBonus=0,iFlightFuel=0,iWillBonus=25,iLargeItems=1,iSmallItems=2,iMobilityBonus=-2)
Armors=(ABILITIES[0]=eAbility_Fly,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,Properties[0]=eAP_BackpackLimited,Properties[1]=eAP_AirEvade,Properties[2]=eAP_None,Properties[3]=eAP_None,iType=eItem_ArmorArchangel,iHPBonus=9,iDefenseBonus=0,iFlightFuel=6,iWillBonus=15,iLargeItems=1,iSmallItems=2,iMobilityBonus=-2)
Armors=(ABILITIES[0]=eAbility_Ghost,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,Properties[0]=eAP_Grapple,Properties[1]=eAP_None,Properties[2]=eAP_None,Properties[3]=eAP_None,iType=eItem_ArmorGhost,iHPBonus=9,iDefenseBonus=8,iFlightFuel=0,iWillBonus=15,iLargeItems=1,iSmallItems=1,iMobilityBonus=2)
Armors=(ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,Properties[0]=eAP_Psi,Properties[1]=eAP_None,Properties[2]=eAP_None,Properties[3]=eAP_None,iType=eItem_ArmorPsi,iHPBonus=9,iDefenseBonus=8,iFlightFuel=0,iWillBonus=40,iLargeItems=1,iSmallItems=1,iMobilityBonus=2)
Armors=(ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,Properties[0]=eAP_Tank,Properties[1]=eAP_None,Properties[2]=eAP_None,Properties[3]=eAP_None,iType=eItem_SHIVDeck_I,iHPBonus=0,iDefenseBonus=0,iFlightFuel=0,iWillBonus=0,iLargeItems=0,iSmallItems=0,iMobilityBonus=0)
Armors=(ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,Properties[0]=eAP_Tank,Properties[1]=eAP_None,Properties[2]=eAP_None,Properties[3]=eAP_None,iType=eItem_SHIVDeck_II,iHPBonus=0,iDefenseBonus=10,iFlightFuel=0,iWillBonus=0,iLargeItems=0,iSmallItems=0,iMobilityBonus=1)
Armors=(ABILITIES[0]=eAbility_Fly,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,Properties[0]=eAP_Tank,Properties[1]=eAP_None,Properties[2]=eAP_None,Properties[3]=eAP_None,iType=eItem_SHIVDeck_III,iHPBonus=0,iDefenseBonus=20,iFlightFuel=12,iWillBonus=0,iLargeItems=0,iSmallItems=0,iMobilityBonus=3)
Characters=(iType=eChar_None,HP=1,Offense=0,Defense=0,Mobility=12,SightRadius=27,Will=0,Psionics=20,CritHitChance=0,CritWoundChance=0,FlightFuel=0,Reaction=0,ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_None,Properties[1]=eCP_None,Properties[2]=eCP_None,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=False,bCanUse_eTraversal_ClimbOver=False,bCanUse_eTraversal_ClimbOnto=False,bCanUse_eTraversal_ClimbLadder=False,bCanUse_eTraversal_DropDown=False,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=False,bCanUse_eTraversal_BreakWindow=False,bCanUse_eTraversal_KickDoor=False,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_Civilian,HP=1,Offense=0,Defense=0,Mobility=12,SightRadius=27,Will=0,Psionics=20,CritHitChance=0,CritWoundChance=0,FlightFuel=0,Reaction=0,ABILITIES[0]=eAbility_CivilianCover,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_None,Properties[1]=eCP_None,Properties[2]=eCP_None,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=True,bCanUse_eTraversal_ClimbOnto=True,bCanUse_eTraversal_ClimbLadder=True,bCanUse_eTraversal_DropDown=True,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=True,bCanUse_eTraversal_KickDoor=True,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_Soldier,HP=2,Offense=63,Defense=0,Mobility=12,SightRadius=27,Will=30,Psionics=30,CritHitChance=0,CritWoundChance=0,FlightFuel=0,Reaction=0,ABILITIES[0]=eAbility_TakeCover,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_CanGainXP,Properties[1]=eCP_None,Properties[2]=eCP_None,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=True,bCanUse_eTraversal_ClimbOnto=True,bCanUse_eTraversal_ClimbLadder=True,bCanUse_eTraversal_DropDown=True,bCanUse_eTraversal_Grapple=True,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=True,bCanUse_eTraversal_KickDoor=True,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_Tank,HP=10,Offense=70,Defense=20,Mobility=14,SightRadius=27,Will=0,Psionics=20,CritHitChance=0,CritWoundChance=0,FlightFuel=0,Reaction=0,ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_Robotic,Properties[1]=eCP_Hardened,Properties[2]=eCP_None,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=False,bCanUse_eTraversal_ClimbOnto=False,bCanUse_eTraversal_ClimbLadder=False,bCanUse_eTraversal_DropDown=False,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=False,bCanUse_eTraversal_KickDoor=True,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_Sectoid,HP=6,Offense=70,Defense=9,Mobility=9,SightRadius=27,Will=40,Psionics=20,CritHitChance=0,CritWoundChance=0,FlightFuel=0,Reaction=0,ABILITIES[0]=eAbility_TakeCover,ABILITIES[1]=eAbility_MindMerge,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_None,Properties[1]=eCP_None,Properties[2]=eCP_None,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=True,bCanUse_eTraversal_ClimbOnto=True,bCanUse_eTraversal_ClimbLadder=True,bCanUse_eTraversal_DropDown=True,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=True,bCanUse_eTraversal_KickDoor=True,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_Floater,HP=8,Offense=70,Defense=8,Mobility=15,SightRadius=27,Will=40,Psionics=20,CritHitChance=0,CritWoundChance=0,FlightFuel=32,Reaction=0,ABILITIES[0]=eAbility_TakeCover,ABILITIES[1]=eAbility_Fly,ABILITIES[2]=eAbility_Launch,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_Flight,Properties[1]=eCP_AirEvade,Properties[2]=eCP_None,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=True,bCanUse_eTraversal_ClimbOnto=True,bCanUse_eTraversal_ClimbLadder=False,bCanUse_eTraversal_DropDown=false,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=True,bCanUse_eTraversal_KickDoor=True,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_Thinman,HP=6,Offense=75,Defense=7,Mobility=12,SightRadius=27,Will=45,Psionics=20,CritHitChance=0,CritWoundChance=0,FlightFuel=0,Reaction=0,ABILITIES[0]=eAbility_TakeCover,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_Climb,Properties[1]=eCP_PoisonImmunity,Properties[2]=eCP_None,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=True,bCanUse_eTraversal_ClimbOnto=True,bCanUse_eTraversal_ClimbLadder=False,bCanUse_eTraversal_DropDown=True,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=True,bCanUse_eTraversal_KickDoor=True,bCanUse_eTraversal_JumpUp=True,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_Muton,HP=14,Offense=80,Defense=-3,Mobility=12,SightRadius=27,Will=40,Psionics=0,CritHitChance=0,CritWoundChance=0,FlightFuel=0,Reaction=0,ABILITIES[0]=eAbility_TakeCover,ABILITIES[1]=eAbility_Intimidate,ABILITIES[2]=eAbility_BloodCall,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_None,Properties[1]=eCP_None,Properties[2]=eCP_None,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=True,bCanUse_eTraversal_ClimbOnto=True,bCanUse_eTraversal_ClimbLadder=True,bCanUse_eTraversal_DropDown=True,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=True,bCanUse_eTraversal_KickDoor=True,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_Cyberdisc,HP=22,Offense=80,Defense=2,Mobility=18,SightRadius=27,Will=0,Psionics=20,CritHitChance=0,CritWoundChance=0,FlightFuel=32,Reaction=0,ABILITIES[0]=eAbility_Fly,ABILITIES[1]=eAbility_CloseCyberdisc,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_AirEvade,Properties[1]=eCP_Flight,Properties[2]=eCP_Robotic,Properties[3]=eCP_DeathExplosion,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=False,bCanUse_eTraversal_ClimbOnto=False,bCanUse_eTraversal_ClimbLadder=False,bCanUse_eTraversal_DropDown=False,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=False,bCanUse_eTraversal_KickDoor=False,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_SectoidCommander,HP=11,Offense=80,Defense=10,Mobility=12,SightRadius=27,Will=140,Psionics=75,CritHitChance=0,CritWoundChance=0,FlightFuel=0,Reaction=0,ABILITIES[0]=eAbility_TakeCover,ABILITIES[1]=eAbility_PsiControl,ABILITIES[2]=eAbility_MindFray,ABILITIES[3]=eAbility_GreaterMindMerge,ABILITIES[4]=eAbility_PsiPanic,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_None,Properties[1]=eCP_None,Properties[2]=eCP_None,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=True,bCanUse_eTraversal_ClimbOnto=True,bCanUse_eTraversal_ClimbLadder=True,bCanUse_eTraversal_DropDown=True,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=True,bCanUse_eTraversal_KickDoor=True,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_FloaterHeavy,HP=16,Offense=75,Defense=5,Mobility=15,SightRadius=27,Will=55,Psionics=20,CritHitChance=0,CritWoundChance=0,FlightFuel=32,Reaction=0,ABILITIES[0]=eAbility_TakeCover,ABILITIES[1]=eAbility_Fly,ABILITIES[2]=eAbility_Launch,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_Flight,Properties[1]=eCP_AirEvade,Properties[2]=eCP_None,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=False,bCanUse_eTraversal_ClimbOnto=False,bCanUse_eTraversal_ClimbLadder=False,bCanUse_eTraversal_DropDown=False,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=False,bCanUse_eTraversal_KickDoor=False,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_MutonElite,HP=21,Offense=85,Defense=-3,Mobility=12,SightRadius=27,Will=50,Psionics=0,CritHitChance=0,CritWoundChance=0,FlightFuel=0,Reaction=0,ABILITIES[0]=eAbility_TakeCover,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_None,Properties[1]=eCP_None,Properties[2]=eCP_None,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=True,bCanUse_eTraversal_ClimbOnto=True,bCanUse_eTraversal_ClimbLadder=True,bCanUse_eTraversal_DropDown=True,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=True,bCanUse_eTraversal_KickDoor=True,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_Ethereal,HP=22,Offense=100,Defense=30,Mobility=12,SightRadius=27,Will=160,Psionics=120,CritHitChance=0,CritWoundChance=0,FlightFuel=0,Reaction=0,ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_Rift,ABILITIES[2]=eAbility_PsiControl,ABILITIES[3]=eAbility_PsiDrain,ABILITIES[4]=eAbility_MindFray,ABILITIES[5]=eAbility_None,ABILITIES[6]=eAbility_None,ABILITIES[7]=eAbility_None,Properties[0]=eCP_Hardened,Properties[1]=eCP_None,Properties[2]=eCP_DeathExplosion,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=True,bCanUse_eTraversal_ClimbOnto=True,bCanUse_eTraversal_ClimbLadder=False,bCanUse_eTraversal_DropDown=True,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=True,bCanUse_eTraversal_KickDoor=True,bCanUse_eTraversal_JumpUp=True,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_EtherealUber,HP=40,Offense=120,Defense=30,Mobility=12,SightRadius=27,Will=200,Psionics=80,CritHitChance=0,CritWoundChance=0,FlightFuel=0,Reaction=0,ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_Rift,ABILITIES[2]=eAbility_PsiControl,ABILITIES[3]=eAbility_PsiDrain,ABILITIES[4]=eAbility_MindFray,ABILITIES[5]=eAbility_None,ABILITIES[6]=eAbility_None,ABILITIES[7]=eAbility_None,Properties[0]=eCP_Hardened,Properties[1]=eCP_None,Properties[2]=eCP_DeathExplosion,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=True,bCanUse_eTraversal_ClimbOnto=True,bCanUse_eTraversal_ClimbLadder=True,bCanUse_eTraversal_DropDown=True,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=True,bCanUse_eTraversal_KickDoor=True,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_Chryssalid,HP=11,Offense=0,Defense=0,Mobility=20,SightRadius=27,Will=150,Psionics=20,CritHitChance=0,CritWoundChance=0,FlightFuel=0,Reaction=0,ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_MeleeOnly,Properties[1]=eCP_Hardened,Properties[2]=eCP_None,Properties[3]=eCP_PoisonImmunity,Properties[4]=eCP_Poisonous,Properties[5]=eCP_Climb,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=True,bCanUse_eTraversal_ClimbOnto=True,bCanUse_eTraversal_ClimbLadder=False,bCanUse_eTraversal_DropDown=True,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=True,bCanUse_eTraversal_KickDoor=True,bCanUse_eTraversal_JumpUp=True,bCanUse_eTraversal_WallClimb=True,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_Zombie,HP=12,Offense=0,Defense=0,Mobility=8,SightRadius=27,Will=150,Psionics=20,CritHitChance=0,CritWoundChance=0,FlightFuel=0,Reaction=0,ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_PoisonImmunity,Properties[1]=eCP_MeleeOnly,Properties[2]=eCP_Poisonous,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=True,bCanUse_eTraversal_ClimbOnto=True,bCanUse_eTraversal_ClimbLadder=True,bCanUse_eTraversal_DropDown=True,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=True,bCanUse_eTraversal_KickDoor=True,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_MutonBerserker,HP=25,Offense=80,Defense=-3,Mobility=17,SightRadius=27,Will=110,Psionics=0,CritHitChance=0,CritWoundChance=0,FlightFuel=0,Reaction=0,ABILITIES[0]=eAbility_BullRush,ABILITIES[1]=eAbility_Bloodlust,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_MeleeOnly,Properties[1]=eCP_Hardened,Properties[2]=eCP_None,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=True,bCanUse_eTraversal_ClimbOnto=True,bCanUse_eTraversal_ClimbLadder=True,bCanUse_eTraversal_DropDown=True,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=True,bCanUse_eTraversal_KickDoor=True,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_Sectopod,HP=40,Offense=80,Defense=-5,Mobility=12,SightRadius=27,Will=0,Psionics=20,CritHitChance=0,CritWoundChance=0,FlightFuel=0,Reaction=0,ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_Robotic,Properties[1]=eCP_Hardened,Properties[2]=eCP_DeathExplosion,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=False,bCanUse_eTraversal_ClimbOnto=False,bCanUse_eTraversal_ClimbLadder=False,bCanUse_eTraversal_DropDown=False,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=False,bCanUse_eTraversal_KickDoor=True,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_Drone,HP=8,Offense=75,Defense=20,Mobility=12,SightRadius=27,Will=0,Psionics=20,CritHitChance=0,CritWoundChance=0,FlightFuel=32,Reaction=0,ABILITIES[0]=eAbility_Fly,ABILITIES[1]=eAbility_ShotOverload,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_Robotic,Properties[1]=eCP_Flight,Properties[2]=eCP_Hardened,Properties[3]=eCP_AirEvade,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=False,bCanUse_eTraversal_ClimbOnto=False,bCanUse_eTraversal_ClimbLadder=False,bCanUse_eTraversal_DropDown=False,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=True,bCanUse_eTraversal_KickDoor=True,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_Outsider,HP=6,Offense=80,Defense=0,Mobility=12,SightRadius=27,Will=60,Psionics=20,CritHitChance=0,CritWoundChance=0,FlightFuel=32,Reaction=0,ABILITIES[0]=eAbility_TakeCover,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_None,Properties[1]=eCP_None,Properties[2]=eCP_None,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=True,bCanUse_eTraversal_ClimbOver=False,bCanUse_eTraversal_ClimbOnto=False,bCanUse_eTraversal_ClimbLadder=False,bCanUse_eTraversal_DropDown=False,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=True,bCanUse_eTraversal_BreakWindow=True,bCanUse_eTraversal_KickDoor=True,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_BattleScanner,HP=10,Offense=0,Defense=0,Mobility=0,SightRadius=18,Will=0,Psionics=0,CritHitChance=0,CritWoundChance=0,FlightFuel=0,Reaction=0,ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_PoisonImmunity,Properties[1]=eCP_None,Properties[2]=eCP_None,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=False,bCanUse_eTraversal_ClimbOver=False,bCanUse_eTraversal_ClimbOnto=False,bCanUse_eTraversal_ClimbLadder=False,bCanUse_eTraversal_DropDown=False,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=False,bCanUse_eTraversal_BreakWindow=False,bCanUse_eTraversal_KickDoor=False,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Characters=(iType=eChar_None,HP=0,Offense=0,Defense=0,Mobility=0,SightRadius=0,Will=0,Psionics=0,CritHitChance=0,CritWoundChance=0,FlightFuel=0,Reaction=0,ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,ABILITIES[6]=eAbility_NONE,ABILITIES[7]=eAbility_NONE,Properties[0]=eCP_None,Properties[1]=eCP_None,Properties[2]=eCP_None,Properties[3]=eCP_None,Properties[4]=eCP_None,Properties[5]=eCP_None,bCanUse_eTraversal_Normal=False,bCanUse_eTraversal_ClimbOver=False,bCanUse_eTraversal_ClimbOnto=False,bCanUse_eTraversal_ClimbLadder=False,bCanUse_eTraversal_DropDown=False,bCanUse_eTraversal_Grapple=False,bCanUse_eTraversal_Landing=False,bCanUse_eTraversal_BreakWindow=False,bCanUse_eTraversal_KickDoor=False,bCanUse_eTraversal_JumpUp=False,bCanUse_eTraversal_WallClimb=False,bCanUse_eTraversal_BreakWall=False,eClass=eSC_None,bHasPsiGift=False)
Weapons=(iType=eItem_Pistol,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_Overwatch,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Pistol,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=2,iEnvironmentDamage=10,iRange=27,iReactionRange=-1,iReactionAngle=270,iRadius=0,iCritical=5,iOffenseBonus=-5,iSuppression=5,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_AssaultRifle,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_Overwatch,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Rifle,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=5,iEnvironmentDamage=30,iRange=27,iReactionRange=-1,iReactionAngle=270,iRadius=0,iCritical=10,iOffenseBonus=0,iSuppression=10,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_Shotgun,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_Overwatch,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Assault,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=7,iEnvironmentDamage=40,iRange=27,iReactionRange=-1,iReactionAngle=360,iRadius=0,iCritical=20,iOffenseBonus=-5,iSuppression=10,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_LMG,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_Overwatch,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_Overwatch,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Heavy,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=6,iEnvironmentDamage=50,iRange=27,iReactionRange=-1,iReactionAngle=270,iRadius=0,iCritical=5,iOffenseBonus=0,iSuppression=15,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_SniperRifle,ABILITIES[0]=eAbility_Overwatch,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_ShotStandard,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Sniper,Properties[1]=eWP_MoveLimited,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=6,iEnvironmentDamage=20,iRange=100,iReactionRange=-1,iReactionAngle=270,iRadius=0,iCritical=25,iOffenseBonus=3,iSuppression=5,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_LaserPistol,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_Overwatch,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Pistol,Properties[1]=eWP_UnlimitedAmmo,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=1,iEnvironmentDamage=20,iRange=27,iReactionRange=-1,iReactionAngle=270,iRadius=0,iCritical=80,iOffenseBonus=5,iSuppression=5,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=4)
Weapons=(iType=eItem_LaserAssaultRifle,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_Overwatch,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Rifle,Properties[1]=eWP_UnlimitedAmmo,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=3,iEnvironmentDamage=40,iRange=27,iReactionRange=-1,iReactionAngle=270,iRadius=0,iCritical=80,iOffenseBonus=10,iSuppression=10,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=4)
Weapons=(iType=eItem_LaserAssaultGun,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_Overwatch,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Assault,Properties[1]=eWP_UnlimitedAmmo,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=4,iEnvironmentDamage=50,iRange=27,iReactionRange=-1,iReactionAngle=360,iRadius=0,iCritical=85,iOffenseBonus=5,iSuppression=10,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=4)
Weapons=(iType=eItem_ArcThrower,ABILITIES[0]=eAbility_ShotStun,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Pistol,Properties[1]=eWP_Secondary,Properties[2]=eWP_CantReact,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=0,iEnvironmentDamage=0,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_HeavyLaser,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_Overwatch,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Heavy,Properties[1]=eWP_UnlimitedAmmo,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=4,iEnvironmentDamage=60,iRange=27,iReactionRange=30,iReactionAngle=270,iRadius=0,iCritical=80,iOffenseBonus=8,iSuppression=15,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=4)
Weapons=(iType=eItem_LaserSniperRifle,ABILITIES[0]=eAbility_Overwatch,ABILITIES[1]=eAbility_ShotStandard,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Sniper,Properties[1]=eWP_UnlimitedAmmo,Properties[2]=eWP_MoveLimited,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=4,iEnvironmentDamage=40,iRange=100,iReactionRange=-1,iReactionAngle=270,iRadius=0,iCritical=85,iOffenseBonus=12,iSuppression=5,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=4)
Weapons=(iType=eItem_PlasmaPistol,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_Overwatch,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Pistol,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=3,iEnvironmentDamage=40,iRange=27,iReactionRange=-1,iReactionAngle=270,iRadius=0,iCritical=10,iOffenseBonus=-15,iSuppression=10,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=5)
Weapons=(iType=eItem_SectoidPlasmaPistol,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_ShotSuppress,ABILITIES[2]=eAbility_Overwatch,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Pistol,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=4,iEnvironmentDamage=40,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=-15,iSuppression=10,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_PlasmaLightRifle,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_Overwatch,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Rifle,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=6,iEnvironmentDamage=55,iRange=27,iReactionRange=-1,iReactionAngle=270,iRadius=0,iCritical=15,iOffenseBonus=-5,iSuppression=20,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=5)
Weapons=(iType=eItem_PlasmaLightRifle_ThinMan,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_ShotSuppress,ABILITIES[2]=eAbility_Overwatch,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Rifle,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=8,iEnvironmentDamage=55,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=-11,iSuppression=20,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_PlasmaLightRifle_Floater,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_ShotSuppress,ABILITIES[2]=eAbility_Overwatch,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Rifle,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=8,iEnvironmentDamage=55,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=-11,iSuppression=20,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_PlasmaLightRifle_Muton,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_ShotSuppress,ABILITIES[2]=eAbility_Overwatch,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Rifle,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=8,iEnvironmentDamage=55,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=-11,iSuppression=20,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_PlasmaAssaultRifle,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_Overwatch,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Rifle,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=8,iEnvironmentDamage=65,iRange=27,iReactionRange=-1,iReactionAngle=270,iRadius=0,iCritical=17,iOffenseBonus=-12,iSuppression=20,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=5)
Weapons=(iType=eItem_PlasmaAssaultRifle_Muton,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_ShotSuppress,ABILITIES[2]=eAbility_Overwatch,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Rifle,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=9,iEnvironmentDamage=75,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=-12,iSuppression=20,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_AlloyCannon,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_Overwatch,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Assault,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=9,iEnvironmentDamage=70,iRange=27,iReactionRange=-1,iReactionAngle=360,iRadius=0,iCritical=25,iOffenseBonus=-15,iSuppression=35,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=5)
Weapons=(iType=eItem_HeavyPlasma,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_Overwatch,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Heavy,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=8,iEnvironmentDamage=75,iRange=27,iReactionRange=-1,iReactionAngle=270,iRadius=0,iCritical=10,iOffenseBonus=-13,iSuppression=35,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=5)
Weapons=(iType=eItem_HeavyPlasma_Floater,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_ShotSuppress,ABILITIES[2]=eAbility_Overwatch,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Heavy,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=10,iEnvironmentDamage=100,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=-13,iSuppression=35,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_HeavyPlasma_Muton,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_ShotSuppress,ABILITIES[2]=eAbility_Overwatch,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Heavy,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=10,iEnvironmentDamage=100,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=-13,iSuppression=35,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_PlasmaSniperRifle,ABILITIES[0]=eAbility_Overwatch,ABILITIES[1]=eAbility_ShotStandard,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Sniper,Properties[1]=eWP_MoveLimited,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=9,iEnvironmentDamage=55,iRange=100,iReactionRange=-1,iReactionAngle=270,iRadius=0,iCritical=35,iOffenseBonus=-10,iSuppression=10,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=5)
Weapons=(iType=eItem_FragGrenade,ABILITIES[0]=eAbility_FragGrenade,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_AnyClass,Properties[1]=eWP_Secondary,Properties[2]=eWP_NoReload,Properties[3]=eWP_Explosive,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=4,iEnvironmentDamage=250,iRange=15,iReactionRange=-1,iReactionAngle=200,iRadius=240,iCritical=0,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_SmokeGrenade,ABILITIES[0]=eAbility_SmokeGrenade,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_AnyClass,Properties[1]=eWP_Secondary,Properties[2]=eWP_NoReload,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=-1,iEnvironmentDamage=0,iRange=15,iReactionRange=-1,iReactionAngle=200,iRadius=336,iCritical=0,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_BattleScanner,ABILITIES[0]=eAbility_BattleScanner,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_AnyClass,Properties[1]=eWP_Secondary,Properties[2]=eWP_NoReload,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=-1,iEnvironmentDamage=0,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=225,iCritical=0,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_AlienGrenade,ABILITIES[0]=eAbility_AlienGrenade,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_AnyClass,Properties[1]=eWP_Secondary,Properties[2]=eWP_NoReload,Properties[3]=eWP_Explosive,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=5,iEnvironmentDamage=250,iRange=17,iReactionRange=-1,iReactionAngle=200,iRadius=240,iCritical=0,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_PsiGrenade,ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_AnyClass,Properties[1]=eWP_Secondary,Properties[2]=eWP_NoReload,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=6,iEnvironmentDamage=250,iRange=15,iReactionRange=-1,iReactionAngle=200,iRadius=225,iCritical=25,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_RocketLauncher,ABILITIES[0]=eAbility_ShredderRocket,ABILITIES[1]=eAbility_RocketLauncher,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Explosive,Properties[1]=eWP_Secondary,Properties[2]=eWP_NoReload,Properties[3]=eWP_Heavy,Properties[4]=eWP_MoveLimited,Properties[5]=eWP_None,iDamage=6,iEnvironmentDamage=500,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=336,iCritical=0,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_BlasterLauncher,ABILITIES[0]=eAbility_ShredderRocket,ABILITIES[1]=eAbility_RocketLauncher,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Explosive,Properties[1]=eWP_Secondary,Properties[2]=eWP_NoReload,Properties[3]=eWP_Heavy,Properties[4]=eWP_MoveLimited,Properties[5]=eWP_None,iDamage=9,iEnvironmentDamage=500,iRange=100,iReactionRange=-1,iReactionAngle=200,iRadius=336,iCritical=0,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_Medikit,ABILITIES[0]=eAbility_MedikitHeal,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_AnyClass,Properties[1]=eWP_Secondary,Properties[2]=eWP_NoReload,Properties[3]=eWP_Backpack,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=-1,iEnvironmentDamage=0,iRange=3,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_Plague,ABILITIES[0]=eAbility_Plague,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Support,Properties[1]=eWP_Secondary,Properties[2]=eWP_NoReload,Properties[3]=eWP_Backpack,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=6,iEnvironmentDamage=0,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=144,iCritical=0,iOffenseBonus=0,iSuppression=20,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_CombatStims,ABILITIES[0]=eAbility_CombatStim,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_AnyClass,Properties[1]=eWP_Secondary,Properties[2]=eWP_Backpack,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=-1,iEnvironmentDamage=0,iRange=3,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_ShivMinigun,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_Overwatch,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Integrated,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=7,iEnvironmentDamage=50,iRange=27,iReactionRange=-1,iReactionAngle=270,iRadius=0,iCritical=10,iOffenseBonus=0,iSuppression=20,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_ShivSentry,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_Overwatch,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Integrated,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=9,iEnvironmentDamage=65,iRange=27,iReactionRange=-1,iReactionAngle=270,iRadius=0,iCritical=10,iOffenseBonus=-5,iSuppression=30,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_ShivLaser,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_Overwatch,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Integrated,Properties[1]=eWP_UnlimitedAmmo,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=4,iEnvironmentDamage=60,iRange=27,iReactionRange=-1,iReactionAngle=270,iRadius=0,iCritical=90,iOffenseBonus=10,iSuppression=25,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_ShivPlasma,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_Overwatch,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Integrated,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=10,iEnvironmentDamage=75,iRange=27,iReactionRange=-1,iReactionAngle=270,iRadius=0,iCritical=20,iOffenseBonus=-10,iSuppression=30,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_Grapple,ABILITIES[0]=eAbility_Grapple,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Integrated,Properties[1]=eWP_Secondary,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=-1,iEnvironmentDamage=-1,iRange=17,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_DroneBeam,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_Repair,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Integrated,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=3,iEnvironmentDamage=2,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_CyberdiscWeapon,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_DeathBlossom,ABILITIES[2]=eAbility_Overwatch,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Integrated,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=9,iEnvironmentDamage=250,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=10,iOffenseBonus=0,iSuppression=3,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_ChryssalidClaw,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Integrated,Properties[1]=eWP_Melee,Properties[2]=eWP_CantReact,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=7,iEnvironmentDamage=1,iRange=2,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=20,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_ZombieFist,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Integrated,Properties[1]=eWP_Melee,Properties[2]=eWP_CantReact,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=9,iEnvironmentDamage=1,iRange=2,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_MutonBlade,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Integrated,Properties[1]=eWP_Melee,Properties[2]=eWP_CantReact,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=10,iEnvironmentDamage=125,iRange=2,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=33,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_SectopodChestCannon,ABILITIES[0]=eAbility_CannonFire,ABILITIES[1]=eAbility_DestroyTerrain,ABILITIES[2]=eAbility_Overwatch,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Integrated,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=10,iEnvironmentDamage=801,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=175,iCritical=0,iOffenseBonus=0,iSuppression=40,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_SectopodCannon,ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Integrated,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=8,iEnvironmentDamage=250,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=175,iCritical=0,iOffenseBonus=0,iSuppression=40,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_SectopodClusterBomb,ABILITIES[0]=eAbility_ClusterBomb,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_AnyClass,Properties[1]=eWP_Secondary,Properties[2]=eWP_NoReload,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=6,iEnvironmentDamage=250,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=144,iCritical=25,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_SectopodHeatRay,ABILITIES[0]=eAbility_Overwatch,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Integrated,Properties[1]=eWP_Secondary,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=20,iEnvironmentDamage=801,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=25,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_PsiAmp,ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Integrated,Properties[1]=eWP_Secondary,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=10,iEnvironmentDamage=400,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_ChitinPlating,ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Backpack,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=0,iEnvironmentDamage=0,iRange=0,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=6,iWillBonus=10)
Weapons=(iType=eItem_ReinforcedArmor,ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Backpack,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=0,iEnvironmentDamage=0,iRange=0,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=4,iWillBonus=5)
Weapons=(iType=eItem_TargetingModule,ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Backpack,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=0,iEnvironmentDamage=0,iRange=0,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=10,iSuppression=0,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_MindShield,ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Backpack,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=0,iEnvironmentDamage=0,iRange=0,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=30)
Weapons=(iType=eItem_ElderWeapon,ABILITIES[0]=eAbility_PsiLance,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Integrated,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=10,iEnvironmentDamage=0,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_OutsiderWeapon,ABILITIES[0]=eAbility_ShotStandard,ABILITIES[1]=eAbility_ShotSuppress,ABILITIES[2]=eAbility_Overwatch,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Rifle,Properties[1]=eWP_None,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=8,iEnvironmentDamage=0,iRange=27,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=0,iSuppression=30,iSize=eItemSize_Large,iHPBonus=0,iWillBonus=0)
Weapons=(iType=eItem_SoldierNoWeapon,ABILITIES[0]=eAbility_NONE,ABILITIES[1]=eAbility_NONE,ABILITIES[2]=eAbility_NONE,ABILITIES[3]=eAbility_NONE,ABILITIES[4]=eAbility_NONE,ABILITIES[5]=eAbility_NONE,Properties[0]=eWP_Pistol,Properties[1]=eWP_UnlimitedAmmo,Properties[2]=eWP_None,Properties[3]=eWP_None,Properties[4]=eWP_None,Properties[5]=eWP_None,iDamage=0,iEnvironmentDamage=0,iRange=10,iReactionRange=-1,iReactionAngle=200,iRadius=0,iCritical=0,iOffenseBonus=0,iSuppression=0,iSize=eItemSize_Small,iHPBonus=0,iWillBonus=0)

BalanceMods_Easy=(eType=eChar_None,				iDamage=0, iCritHit=0,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_Civilian,			iDamage=0, iCritHit=0,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_Soldier,			iDamage=0, iCritHit=0,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_Tank,				iDamage=0, iCritHit=0,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_Sectoid,			iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_Floater,			iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_Thinman,			iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_Muton,			iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_Cyberdisc,		iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_SectoidCommander,	iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_FloaterHeavy,		iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_MutonElite,		iDamage=0, iCritHit=15,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_Ethereal,			iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_Chryssalid,		iDamage=0, iCritHit=25,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_Zombie,			iDamage=0, iCritHit=25,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_MutonBerserker,	iDamage=0, iCritHit=40,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_Sectopod,			iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_Drone,			iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_Outsider,			iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_EtherealUber,		iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Easy=(eType=eChar_BattleScanner,	iDamage=0, iCritHit=0,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)

BalanceMods_Normal=(eType=eChar_None,				iDamage=0, iCritHit=0,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_Civilian,			iDamage=0, iCritHit=0,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_Soldier,			iDamage=0, iCritHit=0,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_Tank,				iDamage=0, iCritHit=0,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_Sectoid,			iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_Floater,			iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_Thinman,			iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_Muton,				iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_Cyberdisc,			iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_SectoidCommander,	iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_FloaterHeavy,		iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_MutonElite,			iDamage=0, iCritHit=15,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_Ethereal,			iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_Chryssalid,			iDamage=0, iCritHit=25,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_Zombie,				iDamage=0, iCritHit=25,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_MutonBerserker,		iDamage=0, iCritHit=40,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_Sectopod,			iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_Drone,				iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_Outsider,			iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_EtherealUber,		iDamage=0, iCritHit=10,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)
BalanceMods_Normal=(eType=eChar_BattleScanner,		iDamage=0, iCritHit=0,iAim=0,iDefense=0,iHP=0, iMobility=0,iWill=0)

BalanceMods_Hard=(eType=eChar_None,				iDamage=0, iCritHit=0, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_Civilian,			iDamage=0, iCritHit=0, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_Soldier,			iDamage=0, iCritHit=0, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_Tank,				iDamage=0, iCritHit=0, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_Sectoid,			iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_Floater,			iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_Thinman,			iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_Muton,			iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_Cyberdisc,		iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_SectoidCommander,	iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_FloaterHeavy,		iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_MutonElite,		iDamage=0, iCritHit=15, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_Ethereal,			iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_Chryssalid,		iDamage=0, iCritHit=25, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_Zombie,			iDamage=0, iCritHit=25, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_MutonBerserker,	iDamage=0, iCritHit=40, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_Sectopod,			iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_Drone,			iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_Outsider,			iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_EtherealUber,		iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Hard=(eType=eChar_BattleScanner,	iDamage=0, iCritHit=0, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)

BalanceMods_Classic=(eType=eChar_None,				iDamage=0, iCritHit=0, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_Civilian,			iDamage=0, iCritHit=0, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_Soldier,			iDamage=0, iCritHit=0, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_Tank,				iDamage=0, iCritHit=0, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_Sectoid,			iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_Floater,			iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_Thinman,			iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_Muton,				iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_Cyberdisc,			iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_SectoidCommander,	iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_FloaterHeavy,		iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_MutonElite,		iDamage=0, iCritHit=15, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_Ethereal,			iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_Chryssalid,		iDamage=0, iCritHit=25, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_Zombie,			iDamage=0, iCritHit=25, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_MutonBerserker,	iDamage=0, iCritHit=40, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_Sectopod,			iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_Drone,				iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_Outsider,			iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_EtherealUber,		iDamage=0, iCritHit=10, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)
BalanceMods_Classic=(eType=eChar_BattleScanner,		iDamage=0, iCritHit=0, iAim=0,iDefense=0, iHP=0, iMobility=0,iWill=0)

iRandWillIncrease=3
iBaseOTSWillIncrease=2
iRandOTSWillIncrease=2
SoldierStatProgression=(eClass=eSC_Support,		 eRank=eRank_Squaddie,	iHP=0, iAim=3, iWill=5, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_Support,		 eRank=eRank_Corporal,	iHP=0, iAim=3, iWill=4, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_Support,		 eRank=eRank_Sergeant,	iHP=1, iAim=3, iWill=2, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_Support,		 eRank=eRank_Lieutenant,iHP=0, iAim=3, iWill=1, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_Support,		 eRank=eRank_Captain,	iHP=0, iAim=2, iWill=1, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_Support,		 eRank=eRank_Major,		iHP=1, iAim=1, iWill=1, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_Support,		 eRank=eRank_Colonel,	iHP=0, iAim=1, iWill=1, iDefense=0, iMobility=0 )

SoldierStatProgression=(eClass=eSC_HeavyWeapons, eRank=eRank_Squaddie,	iHP=0, iAim=3, iWill=5, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_HeavyWeapons, eRank=eRank_Corporal,	iHP=0, iAim=3, iWill=4, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_HeavyWeapons, eRank=eRank_Sergeant,	iHP=1, iAim=3, iWill=2, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_HeavyWeapons, eRank=eRank_Lieutenant,iHP=0, iAim=2, iWill=1, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_HeavyWeapons, eRank=eRank_Captain,	iHP=0, iAim=2, iWill=1, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_HeavyWeapons, eRank=eRank_Major,		iHP=1, iAim=1, iWill=1, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_HeavyWeapons, eRank=eRank_Colonel,	iHP=0, iAim=1, iWill=1, iDefense=0, iMobility=0 )

SoldierStatProgression=(eClass=eSC_Assault,		 eRank=eRank_Squaddie,	iHP=0, iAim=3, iWill=5, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_Assault,		 eRank=eRank_Corporal,	iHP=0, iAim=3, iWill=4, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_Assault,		 eRank=eRank_Sergeant,	iHP=1, iAim=4, iWill=2, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_Assault,		 eRank=eRank_Lieutenant,iHP=0, iAim=3, iWill=1, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_Assault,		 eRank=eRank_Captain,	iHP=0, iAim=2, iWill=1, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_Assault,		 eRank=eRank_Major,		iHP=1, iAim=1, iWill=1, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_Assault,		 eRank=eRank_Colonel,	iHP=0, iAim=1, iWill=1, iDefense=0, iMobility=0 )

SoldierStatProgression=(eClass=eSC_Sniper,		 eRank=eRank_Squaddie,	iHP=0, iAim=3, iWill=5, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_Sniper,		 eRank=eRank_Corporal,	iHP=0, iAim=3, iWill=4, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_Sniper,		 eRank=eRank_Sergeant,	iHP=1, iAim=4, iWill=2, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_Sniper,		 eRank=eRank_Lieutenant,iHP=0, iAim=4, iWill=1, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_Sniper,		 eRank=eRank_Captain,	iHP=0, iAim=3, iWill=1, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_Sniper,		 eRank=eRank_Major,		iHP=1, iAim=2, iWill=1, iDefense=0, iMobility=0 )
SoldierStatProgression=(eClass=eSC_Sniper,		 eRank=eRank_Colonel,	iHP=0, iAim=1, iWill=1, iDefense=0, iMobility=0 )

; Funding Multiplier by Difficulty Level (Easy,Normal,Classic,Impossible)
FundingBalance=1.0
FundingBalance=1
FundingBalance=1
FundingBalance=1
; Country Funding Amounts
FundingAmounts=150 ;USA
FundingAmounts=140 ;Russia
FundingAmounts=110 ;China
FundingAmounts=100 ;UK
FundingAmounts=140 ;Germany
FundingAmounts=90 ;France
FundingAmounts=120 ;Japan
FundingAmounts=90 ;India
FundingAmounts=90 ;Australia
FundingAmounts=0 ;Italy
FundingAmounts=0 ;SouthKorea
FundingAmounts=0 ;Turkey
FundingAmounts=0 ;Indonesia
FundingAmounts=0 ;Spain
FundingAmounts=0 ;Pakistan
FundingAmounts=100 ;Canada
FundingAmounts=0 ;Iran
FundingAmounts=0 ;Israel
FundingAmounts=80 ;Egypt
FundingAmounts=130 ;Brazil
FundingAmounts=100 ;Argentina
FundingAmounts=70 ;Mexico 
FundingAmounts=120 ;SouthAfrica
FundingAmounts=0 ;SaudiArabia
FundingAmounts=0 ;Ukraine
FundingAmounts=130 ;Nigeria
FundingAmounts=0 ;Venezuela
FundingAmounts=0 ;Greece
FundingAmounts=0 ;Columbia
FundingAmounts=0 ;Portugal
FundingAmounts=0 ;Sweden
FundingAmounts=0 ;Ireland
FundingAmounts=0 ;Scotland
FundingAmounts=0 ;Norway
FundingAmounts=0 ;Netherlands
FundingAmounts=0 ;Belgium

ContBalance_Easy=(eCont=eContinent_Africa,			iEngineers1=3, iScientists1=0, iEngineers2=4, iScientists2=1, iEngineers3=5, iScientists3=1 )
ContBalance_Easy=(eCont=eContinent_Asia,			iEngineers1=1, iScientists1=3, iEngineers2=2, iScientists2=3, iEngineers3=3, iScientists3=4, iEngineers4=3, iScientists4=5 )
ContBalance_Easy=(eCont=eContinent_Europe,			iEngineers1=0, iScientists1=3, iEngineers2=1, iScientists2=4, iEngineers3=1, iScientists3=6, iEngineers4=1, iScientists4=7 )
ContBalance_Easy=(eCont=eContinent_NorthAmerica,	iEngineers1=1, iScientists1=2, iEngineers2=2, iScientists2=3, iEngineers3=2, iScientists3=5 )
ContBalance_Easy=(eCont=eContinent_SouthAmerica,	iEngineers1=3, iScientists1=0, iEngineers2=5, iScientists2=1 )

ContBalance_Normal=(eCont=eContinent_Africa,		iEngineers1=3, iScientists1=0, iEngineers2=4, iScientists2=1, iEngineers3=5, iScientists3=1 )
ContBalance_Normal=(eCont=eContinent_Asia,			iEngineers1=1, iScientists1=3, iEngineers2=2, iScientists2=3, iEngineers3=3, iScientists3=4, iEngineers4=3, iScientists4=5 )
ContBalance_Normal=(eCont=eContinent_Europe,		iEngineers1=0, iScientists1=3, iEngineers2=1, iScientists2=4, iEngineers3=1, iScientists3=6, iEngineers4=1, iScientists4=7 )
ContBalance_Normal=(eCont=eContinent_NorthAmerica,	iEngineers1=1, iScientists1=2, iEngineers2=2, iScientists2=3, iEngineers3=2, iScientists3=5 )
ContBalance_Normal=(eCont=eContinent_SouthAmerica,	iEngineers1=3, iScientists1=0, iEngineers2=5, iScientists2=1 )

;ContBalance_Hard=(eCont=eContinent_Africa,			iEngineers1=3, iScientists1=0, iEngineers2=4, iScientists2=1, iEngineers3=5, iScientists3=1 )
;ContBalance_Hard=(eCont=eContinent_Asia,			iEngineers1=1, iScientists1=3, iEngineers2=2, iScientists2=3, iEngineers3=3, iScientists3=4, iEngineers4=3, iScientists4=5 )
;ContBalance_Hard=(eCont=eContinent_Europe,			iEngineers1=0, iScientists1=3, iEngineers2=1, iScientists2=4, iEngineers3=1, iScientists3=6, iEngineers4=1, iScientists4=7 )
;ContBalance_Hard=(eCont=eContinent_NorthAmerica,	iEngineers1=1, iScientists1=2, iEngineers2=2, iScientists2=3, iEngineers3=2, iScientists3=5 )
;ContBalance_Hard=(eCont=eContinent_SouthAmerica,	iEngineers1=3, iScientists1=0, iEngineers2=5, iScientists2=1 )

ContBalance_Hard=(eCont=eContinent_Africa,			iEngineers1=3, iScientists1=0, iEngineers2=4, iScientists2=1, iEngineers3=5, iScientists3=1 )
ContBalance_Hard=(eCont=eContinent_Asia,			iEngineers1=1, iScientists1=3, iEngineers2=2, iScientists2=3, iEngineers3=3, iScientists3=4, iEngineers4=3, iScientists4=5 )
ContBalance_Hard=(eCont=eContinent_Europe,			iEngineers1=0, iScientists1=3, iEngineers2=1, iScientists2=4, iEngineers3=1, iScientists3=6, iEngineers4=1, iScientists4=7 )
ContBalance_Hard=(eCont=eContinent_NorthAmerica,	iEngineers1=1, iScientists1=2, iEngineers2=2, iScientists2=3, iEngineers3=2, iScientists3=5 )
ContBalance_Hard=(eCont=eContinent_SouthAmerica,	iEngineers1=3, iScientists1=0, iEngineers2=5, iScientists2=1 )

ContBalance_Classic=(eCont=eContinent_Africa,		iEngineers1=3, iScientists1=0, iEngineers2=4, iScientists2=1, iEngineers3=5, iScientists3=1 )
ContBalance_Classic=(eCont=eContinent_Asia,			iEngineers1=1, iScientists1=3, iEngineers2=2, iScientists2=3, iEngineers3=3, iScientists3=4, iEngineers4=3, iScientists4=5 )
ContBalance_Classic=(eCont=eContinent_Europe,		iEngineers1=0, iScientists1=3, iEngineers2=1, iScientists2=4, iEngineers3=1, iScientists3=6, iEngineers4=1, iScientists4=7 )
ContBalance_Classic=(eCont=eContinent_NorthAmerica,	iEngineers1=1, iScientists1=2, iEngineers2=2, iScientists2=3, iEngineers3=2, iScientists3=5 )
ContBalance_Classic=(eCont=eContinent_SouthAmerica,	iEngineers1=3, iScientists1=0, iEngineers2=5, iScientists2=1 )


; Psi XP Levels (NONE, 1, 2, 3, Volunteer)
m_iPsiXPLevels=0
m_iPsiXPLevels=0
m_iPsiXPLevels=50
m_iPsiXPLevels=120
m_iPsiXPLevels=10000000

; Soldier XP Levels (Rookie, Squaddie, Corporal, Sergeant, Lieutenant, Captain, Major, Colonel)
m_iSoldierXPLevels=0
m_iSoldierXPLevels=200
m_iSoldierXPLevels=500
m_iSoldierXPLevels=1000
m_iSoldierXPLevels=1700
m_iSoldierXPLevels=2600
m_iSoldierXPLevels=3700
m_iSoldierXPLevels=5000

;Tactical consts
CYBERDISC_ELERIUM=3
HFLOATER_ELERIUM=2
DRONE_ELERIUM=1
SECTOPOD_ELERIUM=7

CYBERDISC_ALLOYS=8
HFLOATER_ALLOYS=4
DRONE_ALLOYS=4
SECTOPOD_ALLOYS=15

MIN_SCATTER=90

SHOOT_WHEN_PANICKED=2

; Chance of offscreen kills resulting in zombies on Chryssalid terror missions
TerrorInfectionChance=1
TerrorInfectionChance=1
TerrorInfectionChance=1
TerrorInfectionChance=1

ThinManPlagueSlider=5
ThinManPlagueSlider=5
ThinManPlagueSlider=5
ThinManPlagueSlider=5

DeathBlossomSlider=10
DeathBlossomSlider=10
DeathBlossomSlider=10
DeathBlossomSlider=10

BloodCallSlider=10
BloodCallSlider=10
BloodCallSlider=10
BloodCallSlider=10

AlienGrenadeSlider=0.20
AlienGrenadeSlider=0.20
AlienGrenadeSlider=0.20
AlienGrenadeSlider=0.20

; Maximum number of aliens that can be attacking you at one time.  -1 == no limit.
MaxActiveAIUnits=-1
MaxActiveAIUnits=-1
MaxActiveAIUnits=-1
MaxActiveAIUnits=-1

CLOSE_RANGE = 14.0f
ASSAULT_LONG_RANGE_MAX_PENALTY = -5
AIM_CLIMB = 3.0f
ASSAULT_AIM_CLIMB = 4f
SNIPER_AIM_FALL = -0.1f

ABDUCTION_LURK_PCT=25
SMALL_UFO_LURK_PCT=0
LARGE_UFO_LURK_PCT=25

GRAPPLE_DIST=15

MAX_CRIT_WOUND=90

FragmentBalance=0.25
FragmentBalance=0.25
FragmentBalance=0.25
FragmentBalance=0.25

; Strategy Game Constants
ABDUCTION_REWARD_CASH=150
ABDUCTION_REWARD_SCI=5
ABDUCTION_REWARD_ENG=4
COUNCIL_DAY=20
COUNCIL_RAND_DAYS=7
COUNCIL_FUNDING_MULTIPLIER_EASY   = 1.0f
COUNCIL_FUNDING_MULTIPLIER_NORMAL = 1
COUNCIL_FUNDING_MULTIPLIER_HARD   = 1
COUNCIL_FUNDING_MULTIPLIER_CLASSIC= 1
ShowUFOsOnMission=1		; Detect the UFOs that are carrying out abductions and terror missions
AI_TERRORIZE_MOST_PANICKED = 65
LATE_UFO_CHANCE = 85	; Chance of Aliens sending out a second UFO in the months after you've defeated the base
EARLY_UFO_CHANCE = 40
UFO_LIMIT = 4
UFO_INTERCEPTION_PCT=90	; Chance that a UFO will require interception
; Main Balance Factors
TECH_TIME_BALANCE=10
ITEM_TIME_BALANCE=1
ITEM_CREDIT_BALANCE=1
ITEM_ELERIUM_BALANCE=1
ITEM_ALLOY_BALANCE=1     
FACILITY_COST_BALANCE=1      
FACILITY_MAINTENANCE_BALANCE=1.9
FACILITY_TIME_BALANCE=1      
ALLOY_UFO_BALANCE=2.0
ALLOY_UFO_BALANCE=2
ALLOY_UFO_BALANCE=2
ALLOY_UFO_BALANCE=2
UFO_ELERIUM_PER_POWER_SOURCE=20
UFO_ELERIUM_PER_POWER_SOURCE=20
UFO_ELERIUM_PER_POWER_SOURCE=20
UFO_ELERIUM_PER_POWER_SOURCE=20

UFOAlloys=35	;eShip_UFOSmallScout
UFOAlloys=39	;eShip_UFOLargeScout
UFOAlloys=117	;eShip_UFOAbductor
UFOAlloys=238	;eShip_UFOSupply
UFOAlloys=216	;eShip_UFOBattle
UFOAlloys=44	;eShip_UFOEthereal

MIN_WRECKED_ALLOYS=0.5
MAX_WRECKED_ALLOYS=0.9
MAX_LOST_WRECKED_ELERIUM=0.75
MIN_LOST_WRECKED_ELERIUM=0.5

; Research/Laboratories
NUM_STARTING_SCIENTISTS=5
LAB_MINIMUM=5
LAB_MULTIPLE=10
LAB_BONUS=1.2
LAB_ADJACENCY_BONUS=1.1
; Engineering/Workshops
NUM_STARTING_ENGINEERS=5
WORKSHOP_MINIMUM=5
WORKSHOP_MULTIPLE=10
WORKSHOP_REBATE_PCT=7
WORKSHOP_ENG_BONUS=5
UPLINK_MULTIPLE=10
NEXUS_MULTIPLE=15
; Soldier Values
NUM_STARTING_SOLDIERS=12
BARRACKS_CAPACITY=99
LOW_AIM=59
HIGH_AIM=67
LOW_MOBILITY=11
HIGH_MOBILITY=12
LOW_WILL=25
HIGH_WILL=35
ROOKIE_AIM=63
ROOKIE_MOBILITY=12 
ROOKIE_STARTING_WILL=30
PSI_GIFT_CHANCE=4       
PSI_TEST_LIMIT=5         
PSI_TRAINING_HOURS=240  
PSI_NUM_TRAINING_SLOTS=3
BASE_DAYS_INJURED = 14
RAND_DAYS_INJURED = 25
SOLDIER_COST=15
SOLDIER_COST_HARD=15
SOLDIER_COST_CLASSIC=15
;Tank Values
BASE_DAYS_INJURED_TANK = 3
RAND_DAYS_INJURED_TANK = 11
ALLOY_SHIV_HP_BONUS = 8
HOVER_SHIV_HP_BONUS = 8
; Headquarters Values
HQ_STARTING_MONEY = 100
BASE_FUNDING = 200
BASE_FUNDING = 200
BASE_FUNDING = 200
BASE_FUNDING = 200
HQ_BASE_POWER = 35
HQ_BASE_POWER = 35
HQ_BASE_POWER = 35
HQ_BASE_POWER = 35
POWER_NORMAL = 6
POWER_THERMAL = 20
POWER_ELERIUM = 30
POWER_ADJACENCY_BONUS = 2
NUM_STARTING_STEAM_VENTS = 3
INTERCEPTOR_REFUEL_RATE=24
INTERCEPTOR_REPAIR_HOURS=216
INTERCEPTOR_REARM_HOURS = 24			
INTERCEPTOR_TRANSFER_TIME = 5		
BASE_SKYRANGER_MAINTENANCE = 20
SKYRANGER_CAPACITY = 4              
UPLINK_CAPACITY = 3
UPLINK_ADJACENCY_BONUS = 1
NEXUS_CAPACITY = 5
; Base Building Values
NUM_TERRAIN_WIDE = 7
NUM_TERRAIN_HIGH = 5
BASE_EXCAVATE_CASH_COST = 10
BASE_REMOVE_CASH_COST = 5
BASE_EXCAVATE_DAYS = 5
BASE_REMOVAL_DAYS = 0
;Mission Timers
UFO_CRASH_TIMER = 48          
TERROR_TIMER = 30             
UFO_LANDED_TIMER = 30         
ABDUCTION_TIMER = 30          
;UFO Values
UFO_PS_SURVIVE = 33
UFO_NAV_SURVIVE = 50
UFO_STASIS_SURVIVE = 25
UFO_SURGERY_SURVIVE = 25
UFO_ENTERTAINMENT_SURVIVE = 25
UFO_FOOD_SURVIVE = 25
UFO_HYPERWAVE_SURVIVE = 100
UFO_FUSION_SURVIVE = 100
UFO_PSI_LINK_SURVIVE = 100    
UFO_FIND_STEALTH_SAT = 10
UFO_FIND_SAT = 85
UFO_SECOND_PASS_FIND_STEALTH_SAT=15
UFO_SECOND_PASS_FIND_SAT=90
;Panic Values
PANIC_TERROR_CONTINENT=2
PANIC_TERROR_COUNTRY=2
PANIC_UFO_SHOOTDOWN=-1
PANIC_UFO_ASSAULT=-1
PANIC_SAT_DESTROYED_CONTINENT=1
PANIC_SAT_DESTROYED_COUNTRY=2
PANIC_SAT_ADDED_COUNTRY=-1
PANIC_SAT_ADDED_CONTINENT=0
PANIC_ALIENBASE_CONQUERED=-2
PANIC_UFO_IGNORED=0
PANIC_UFO_ESCAPED=0
PANIC_ABDUCTION_COUNTRY_EASY=2
PANIC_ABDUCTION_COUNTRY_NORMAL=2
PANIC_ABDUCTION_COUNTRY_HARD=2
PANIC_ABDUCTION_COUNTRY_CLASSIC=2
PANIC_ABDUCTION_CONTINENT_EASY=0
PANIC_ABDUCTION_CONTINENT_NORMAL=0
PANIC_ABDUCTION_CONTINENT_HARD=0
PANIC_ABDUCTION_CONTINENT_CLASSIC=0
PANIC_ABDUCTION_THWARTED_CONTINENT=-1
PANIC_ABDUCTION_THWARTED_COUNTRY=-1
STARTING_PANIC_EASY=0
STARTING_PANIC_NORMAL=0
STARTING_PANIC_HARD=0
STARTING_PANIC_CLASSIC=0	
PANIC_DEFECT_THRESHHOLD_EASY=5
PANIC_DEFECT_THRESHHOLD_NORMAL=5
PANIC_DEFECT_THRESHHOLD_HARD=5
PANIC_DEFECT_THRESHHOLD_CLASSIC=5
PANIC_DEFECT_THRESHHOLD_NOT_HELPED_EASY=4
PANIC_DEFECT_THRESHHOLD_NOT_HELPED_NORMAL=4
PANIC_DEFECT_THRESHHOLD_NOT_HELPED_HARD=4
PANIC_DEFECT_THRESHHOLD_NOT_HELPED_CLASSIC=4
PANIC_DEFECTIONS_PER_MONTH_EASY=2
PANIC_DEFECTIONS_PER_MONTH_NORMAL=2
PANIC_DEFECTIONS_PER_MONTH_HARD=2
PANIC_DEFECTIONS_PER_MONTH_CLASSIC=2
PANIC_DEFECT_CHANCE_PER_BLOCK_EASY=10
PANIC_DEFECT_CHANCE_PER_BLOCK_NORMAL=10
PANIC_DEFECT_CHANCE_PER_BLOCK_HARD=10
PANIC_DEFECT_CHANCE_PER_BLOCK_CLASSIC=10
SAT_HELP_DEFECT=0.5 ;If country has satellite, "Chance of Leaving"*SAT_HELP_DEFECT
SAT_HELP_DEFECT=0.5
SAT_HELP_DEFECT=0.5
SAT_HELP_DEFECT=0.5
SAT_NEARBY_HELP_DEFECT=0.7 ;Else If continent has one or more satellite, "Chance of Leaving"*SAT_NEARBY_HELP_DEFECT
SAT_NEARBY_HELP_DEFECT=0.7
SAT_NEARBY_HELP_DEFECT=0.7
SAT_NEARBY_HELP_DEFECT=0.7
SAT_PANIC_REDUCTION_PER_MONTH=1 ;If a country has a satellite, it will remove panic once a month, but the chance of this happening is PANIC_REDUCTION_CHANCE
SAT_PANIC_REDUCTION_PER_MONTH=1
SAT_PANIC_REDUCTION_PER_MONTH=1
SAT_PANIC_REDUCTION_PER_MONTH=1
PANIC_5_REDUCTION_CHANCE=75
PANIC_5_REDUCTION_CHANCE=75
PANIC_5_REDUCTION_CHANCE=75
PANIC_5_REDUCTION_CHANCE=75

PANIC_4_REDUCTION_CHANCE=50
PANIC_4_REDUCTION_CHANCE=50
PANIC_4_REDUCTION_CHANCE=50
PANIC_4_REDUCTION_CHANCE=50

PANIC_LOW_REDUCTION_CHANCE=15
PANIC_LOW_REDUCTION_CHANCE=15
PANIC_LOW_REDUCTION_CHANCE=15
PANIC_LOW_REDUCTION_CHANCE=15
;Continent Bonuses
CB_FUNDING_BONUS=15
CB_FUTURECOMBAT_BONUS=80
CB_AIRANDSPACE_BONUS=70
CB_EXPERT_BONUS=80
;Second Wave
ENABLE_SECOND_WAVE=1
SW_FLANK_CRIT=100
SW_COVER_INCREASE=1.5f
SW_SATELLITE_INCREASE=1.45f
SW_ELERIUM_HALFLIFE=24
SW_ELERIUM_LOSS=0.05f
SW_ABDUCTION_SITES=4
SW_RARE_PSI=8
SW_MARATHON=1.5f
SW_MORE_POWER=2.0f

SPECIES_POINT_LIMIT=500
KILL_CAM_MIN_DIST=720.0

; Items
ItemBalance=(eItem=eItem_Medikit,	            iCash=25, iElerium=0, iAlloys=0,  iTime=2,	iEng=3)	  
ItemBalance=(eItem=eItem_ArcThrower,			iCash=35, iElerium=0, iAlloys=5,  iTime=6,	iEng=5)	  
ItemBalance=(eItem=eItem_TargetingModule,		iCash=30, iElerium=0, iAlloys=10,  iTime=4,	iEng=5)	  
ItemBalance=(eItem=eItem_ReinforcedArmor,		iCash=20, iElerium=0, iAlloys=0,  iTime=5,	iEng=5)	  
ItemBalance=(eItem=eItem_ChitinPlating,			iCash=85, iElerium=0, iAlloys=70, iTime=15,	iEng=75)	
ItemBalance=(eItem=eItem_CombatStims,			iCash=50, iElerium=0, iAlloys=5,  iTime=3,	iEng=35)	
ItemBalance=(eItem=eItem_MindShield,			iCash=150,iElerium=30,iAlloys=10, iTime=10,	iEng=75)  
ItemBalance=(eItem=eItem_Skeleton_Key,			iCash=25, iElerium=10,iAlloys=20, iTime=7,  iEng=20) 
ItemBalance=(eItem=eItem_LaserPistol,		    iCash=15, iElerium=2, iAlloys=8, iTime=5,  iEng=10) 
ItemBalance=(eItem=eItem_LaserAssaultRifle,		iCash=30, iElerium=4, iAlloys=13, iTime=6,	iEng=12)	
ItemBalance=(eItem=eItem_LaserAssaultGun,		iCash=35, iElerium=4, iAlloys=15, iTime=6,	iEng=12)	
ItemBalance=(eItem=eItem_LaserSniperRifle,		iCash=40, iElerium=6, iAlloys=18, iTime=8,	iEng=20)	
ItemBalance=(eItem=eItem_HeavyLaser,  		    iCash=35, iElerium=6, iAlloys=18, iTime=7,	iEng=20)	
ItemBalance=(eItem=eItem_PlasmaPistol,	        iCash=120,iElerium=10,iAlloys=50, iTime=15,	iEng=60)	
ItemBalance=(eItem=eItem_PlasmaLightRifle,		iCash=125,iElerium=15,iAlloys=70, iTime=17,	iEng=60)  
ItemBalance=(eItem=eItem_PlasmaAssaultRifle,	iCash=300,iElerium=20,iAlloys=130, iTime=17, iEng=90)  
ItemBalance=(eItem=eItem_AlloyCannon,	        iCash=300,iElerium=20,iAlloys=170, iTime=21, iEng=100)  
ItemBalance=(eItem=eItem_HeavyPlasma,	        iCash=375,iElerium=30,iAlloys=150, iTime=17,	iEng=90)  
ItemBalance=(eItem=eItem_PlasmaSniperRifle,		iCash=375,iElerium=25,iAlloys=180, iTime=22, iEng=100) 
ItemBalance=(eItem=eItem_BlasterLauncher,		iCash=400,iElerium=25,iAlloys=190, iTime=22, iEng=100)  
ItemBalance=(eItem=eItem_ArmorCarapace,			iCash=40, iElerium=0, iAlloys=25, iTime=7,	iEng=12) 
ItemBalance=(eItem=eItem_ArmorSkeleton,			iCash=25, iElerium=0, iAlloys=15, iTime=6,	iEng=15) 									
ItemBalance=(eItem=eItem_ArmorTitan,			iCash=250,iElerium=15,iAlloys=300, iTime=15,	iEng=70) 
ItemBalance=(eItem=eItem_ArmorArchAngel,		iCash=300,iElerium=25,iAlloys=350, iTime=16,	iEng=90) 
ItemBalance=(eItem=eItem_ArmorGhost,			iCash=325,iElerium=40,iAlloys=375, iTime=16, iEng=80) 
ItemBalance=(eItem=eItem_ArmorPsi,				iCash=350,iElerium=45,iAlloys=300, iTime=10, iEng=90) 
ItemBalance=(eItem=eItem_SHIV,					iCash=15, iElerium=0, iAlloys=10,  iTime=4,	iEng=5)      
ItemBalance=(eItem=eItem_SHIV_Alloy,			iCash=40, iElerium=0, iAlloys=20, iTime=6,	iEng=30)     
ItemBalance=(eItem=eItem_SHIV_Hover,			iCash=75,iElerium=20,iAlloys=60, iTime=9,	iEng=80)     
ItemBalance=(eItem=eItem_Interceptor,			iCash=30, iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)     
ItemBalance=(eItem=eItem_Firestorm,				iCash=150,iElerium=15,iAlloys=20, iTime=14,	iEng=70)     
ItemBalance=(eItem=eItem_Satellite,				iCash=80,iElerium=0, iAlloys=0,  iTime=20,	iEng=3)	  
ItemBalance=(eItem=eItem_IntWeap_I,				iCash=8,  iElerium=0, iAlloys=0,  iTime=7,	iEng=5)      
ItemBalance=(eItem=eItem_IntWeap_III,			iCash=35, iElerium=0, iAlloys=5, iTime=8,	iEng=10)     
ItemBalance=(eItem=eItem_IntWeap_IV,			iCash=75,iElerium=15,iAlloys=15, iTime=10, iEng=60)     
ItemBalance=(eItem=eItem_IntWeap_V,				iCash=150,iElerium=30,iAlloys=30, iTime=14, iEng=75)     
ItemBalance=(eItem=eItem_IntWeap_VI,			iCash=400,iElerium=7,iAlloys=8, iTime=17, iEng=100)
ItemBalance=(eItem=eItem_IntConsumable_Hit,		iCash=5, iElerium=0, iAlloys=0,  iTime=4,  iEng=5)    
ItemBalance=(eItem=eItem_IntConsumable_Dodge,	iCash=7, iElerium=0, iAlloys=0,  iTime=5,  iEng=10)  
ItemBalance=(eItem=eItem_IntConsumable_Boost,	iCash=5, iElerium=0, iAlloys=0,  iTime=3,  iEng=10)   
ItemBalance=(eItem=eItem_Elerium115,           	iCash=3,  iElerium=1, iAlloys=0,  iTime=-1,	iEng=-1) 
ItemBalance=(eItem=eItem_AlienAlloys,          	iCash=1,  iElerium=0, iAlloys=1,  iTime=-1,	iEng=-1)	
ItemBalance=(eItem=eItem_WeaponFragment,       	iCash=1,  iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)	
ItemBalance=(eItem=eItem_UFOPowerSource,       	iCash=75, iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)	
ItemBalance=(eItem=eItem_UFONavigation,       	iCash=40, iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)	
ItemBalance=(eItem=eItem_AlienFood,            	iCash=10, iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)	
ItemBalance=(eItem=eItem_AlienEntertainment,  	iCash=17, iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)	
ItemBalance=(eItem=eItem_AlienStasisTank,      	iCash=10, iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)	
ItemBalance=(eItem=eItem_AlienSurgery,         	iCash=25, iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)	
ItemBalance=(eItem=eItem_UFOFusionLauncher,    	iCash=125,iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)	
ItemBalance=(eItem=eItem_DamagedUFOPowerSource, iCash=30, iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1) 
ItemBalance=(eItem=eItem_DamagedUFONavigation,  iCash=20, iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1) 
ItemBalance=(eItem=eItem_DamagedAlienFood,      iCash=5,  iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1) 
ItemBalance=(eItem=eItem_DamagedAlienEntertainment,iCash=5,iElerium=0,iAlloys=0,  iTime=-1,	iEng=-1)
ItemBalance=(eItem=eItem_DamagedAlienStasisTank,iCash=5,  iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1) 
ItemBalance=(eItem=eItem_DamagedAlienSurgery,   iCash=10, iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1) 
ItemBalance=(eItem=eItem_DamagedHyperwaveBeacon,iCash=45, iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1) 
ItemBalance=(eItem=eItem_SectoidCorpse,			iCash=5,  iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)	    
ItemBalance=(eItem=eItem_FloaterCorpse,			iCash=5,  iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)	    
ItemBalance=(eItem=eItem_ThinManCorpse,			iCash=5,  iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)	    
ItemBalance=(eItem=eItem_MutonCorpse,			iCash=5,  iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)	    
ItemBalance=(eItem=eItem_CryssalidCorpse,		iCash=20,  iElerium=0, iAlloys=0,  iTime=-1,iEng=-1)	    
ItemBalance=(eItem=eItem_CyberdiscCorpse,		iCash=10,  iElerium=0, iAlloys=0,  iTime=-1,iEng=-1)	    
ItemBalance=(eItem=eItem_SectopodCorpse,		iCash=10, iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)	    
ItemBalance=(eItem=eItem_DroneCorpse,			iCash=2,  iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)	    
ItemBalance=(eItem=eItem_EtherealCorpse,		iCash=15, iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)	    
ItemBalance=(eItem=eItem_SectoidCommanderCorpse,iCash=10, iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)      
ItemBalance=(eItem=eItem_BerserkerCorpse,		iCash=5,  iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)	    
ItemBalance=(eItem=eItem_MutonEliteCorpse,		iCash=8,  iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)      
ItemBalance=(eItem=eItem_FloaterHeavyCorpse,	iCash=6,  iElerium=0, iAlloys=0,  iTime=-1,	iEng=-1)	    

; Facilities
FacilityBalance=( eFacility=eFacility_AccessLift,       iCash=50,	iElerium=0, iAlloys=0,  iMaintenance=10, iTime=5,	iPower=-2)   
FacilityBalance=( eFacility=eFacility_ScienceLab,       iCash=125,  iElerium=0, iAlloys=0,  iMaintenance=24, iTime=10,	iPower=-3)   
FacilityBalance=( eFacility=eFacility_Workshop,         iCash=130,  iElerium=0, iAlloys=0,  iMaintenance=26, iTime=10,	iPower=-3)   
FacilityBalance=( eFacility=eFacility_SmallRadar,       iCash=160,  iElerium=0, iAlloys=0,  iMaintenance=15, iTime=14,	iPower=-5)   
FacilityBalance=( eFacility=eFacility_Power,            iCash=60,   iElerium=0, iAlloys=0,  iMaintenance=11, iTime=5,	iPower=6);MAKE SURE TO SYNC THIS WITH POWER_NORMAL
FacilityBalance=( eFacility=eFacility_Foundry,			iCash=75,   iElerium=0, iAlloys=0,  iMaintenance=20, iTime=10,	iPower=-3)   
FacilityBalance=( eFacility=eFacility_OTS,				iCash=125,  iElerium=0, iAlloys=0,  iMaintenance=25, iTime=8,	iPower=-3)   
FacilityBalance=( eFacility=eFacility_AlienContain,     iCash=85,   iElerium=0, iAlloys=0,  iMaintenance=18, iTime=7,	iPower=-5)   
FacilityBalance=( eFacility=eFacility_LargeRadar,       iCash=300,  iElerium=0, iAlloys=25, iMaintenance=26, iTime=21,	iPower=-6)   
FacilityBalance=( eFacility=eFacility_ThermalPower,     iCash=200,  iElerium=0, iAlloys=0,  iMaintenance=23, iTime=8,	iPower=20);MAKE SURE TO SYNC THIS WITH POWER_THERMAL
FacilityBalance=( eFacility=eFacility_EleriumGenerator,	iCash=275,  iElerium=30,iAlloys=80, iMaintenance=29, iTime=14,	iPower=30);MAKE SURE TO SYNC THIS WITH POWER_ELERIUM
FacilityBalance=( eFacility=eFacility_PsiLabs,          iCash=200,  iElerium=20,iAlloys=50, iMaintenance=30, iTime=14,	iPower=-3)   														   	
FacilityBalance=( eFacility=eFacility_HyperwaveRadar,   iCash=175,  iElerium=0, iAlloys=75, iMaintenance=30, iTime=14,	iPower=-4)  
FacilityBalance=( eFacility=eFacility_DeusEx,           iCash=200,  iElerium=10,iAlloys=40,	iMaintenance=100,iTime=14,	iPower=-5)	 

; Foundry Projects
FoundryBalance=(eTech=eFoundry_SHIV,                  iTime=7, iEngineers=5, iCash=50, iElerium=0, iAlloys=0, iFragments=0, iNumItems=0, eReqTech=eTech_None,				eReqItem=eItem_None)
FoundryBalance=(eTech=eFoundry_AlienGrenades,         iTime=7, iEngineers=75,iCash=175, iElerium=10,iAlloys=50,iFragments=30,iNumItems=1, eReqTech=eTech_Exp_Warfare,		eReqItem=eItem_None)
FoundryBalance=(eTech=eFoundry_SHIV_Heal,             iTime=7, iEngineers=10,iCash=170, iElerium=15,iAlloys=30,iFragments=0, iNumItems=0, eReqTech=eTech_EMP,				eReqItem=eItem_None)
FoundryBalance=(eTech=eFoundry_CaptureDrone,          iTime=14,iEngineers=10,iCash=175,iElerium=0, iAlloys=0, iFragments=10,iNumItems=4, eReqTech=eTech_AutopsyDrone,		eReqItem=eItem_None)
FoundryBalance=(eTech=eFoundry_MedikitII,             iTime=14,iEngineers=15,iCash=125,iElerium=0, iAlloys=0, iFragments=30,iNumItems=4, eReqTech=eTech_AutopsyThinMan,		eReqItem=eItem_None)
FoundryBalance=(eTech=eFoundry_ArcThrowerII,          iTime=14,iEngineers=80,iCash=100,iElerium=20,iAlloys=50,iFragments=35,iNumItems=4, eReqTech=eTech_Elerium,			eReqItem=eItem_None)
FoundryBalance=(eTech=eFoundry_AmmoConservation,      iTime=14,iEngineers=50, iCash=150,iElerium=0, iAlloys=100,iFragments=40,iNumItems=6, eReqTech=eTech_AutopsyMuton,		eReqItem=eItem_None)  
FoundryBalance=(eTech=eFoundry_AutomatedLasers,       iTime=7, iEngineers=15, iCash=100,iElerium=0, iAlloys=15,iFragments=20,iNumItems=0, eReqTech=eTech_HeavyLasers,		eReqItem=eItem_None)
FoundryBalance=(eTech=eFoundry_AutomatedPlasma,       iTime=7, iEngineers=75,iCash=200,iElerium=30,iAlloys=50,iFragments=35,iNumItems=0, eReqTech=eTech_Plasma_Heavy,		eReqItem=eItem_None)
FoundryBalance=(eTech=eFoundry_AdvancedFlight,        iTime=14,iEngineers=100,iCash=325,iElerium=50,iAlloys=80,iFragments=0, iNumItems=2, eReqTech=eTech_Armor_ArchAngel,	eReqItem=eItem_None)
FoundryBalance=(eTech=eFoundry_AdvancedConstruction,  iTime=14,iEngineers=30,iCash=375,iElerium=30,iAlloys=40,iFragments=30,iNumItems=2, eReqTech=eTech_AutopsySectopod,	eReqItem=eItem_None)
FoundryBalance=(eTech=eFoundry_VehicleRepair,         iTime=14,iEngineers=50,iCash=175,iElerium=10,iAlloys=20,iFragments=0, iNumItems=4, eReqTech=eTech_AutopsyHeavyFloater,eReqItem=eItem_None)
FoundryBalance=(eTech=eFoundry_PistolI,               iTime=7, iEngineers=5, iCash=75, iElerium=0, iAlloys=10, iFragments=10, iNumItems=0, eReqTech=eTech_None,				eReqItem=eItem_None)
FoundryBalance=(eTech=eFoundry_PistolII,              iTime=7, iEngineers=50,iCash=150,iElerium=0, iAlloys=30,iFragments=30,iNumItems=0, eReqTech=eTech_LaserWeapons,		eReqItem=eItem_None)
FoundryBalance=(eTech=eFoundry_PistolIII,             iTime=14,iEngineers=80,iCash=250,iElerium=20,iAlloys=50,iFragments=45,iNumItems=0, eReqTech=eTech_Plasma_Pistol,		eReqItem=eItem_None)
FoundryBalance=(eTech=eFoundry_SHIVSuppression,       iTime=7, iEngineers=15,iCash=40, iElerium=0, iAlloys=0, iFragments=10, iNumItems=0, eReqTech=eTech_None,				eReqItem=eItem_None)                  
FoundryBalance=(eTech=eFoundry_StealthSatellites,     iTime=7, iEngineers=80,iCash=550,iElerium=60,iAlloys=100,iFragments=0, iNumItems=3, eReqTech=eTech_AlienNavigation,	eReqItem=eItem_None)
FoundryBalance=(eTech=eFoundry_Scope,                 iTime=7, iEngineers=70,iCash=175, iElerium=10, iAlloys=50, iFragments=40,iNumItems=0, eReqTech=eTech_WeaponFragments,	eReqItem=eItem_None)

; OTS Upgrades
OTSBalance=(eTech=eOTS_Will_I,			iCash=150,	eRank=eRank_Sergeant)
OTSBalance=(eTech=eOTS_SquadSize_I,		iCash=150,	eRank=eRank_Sergeant)
OTSBalance=(eTech=eOTS_XP_I,			iCash=225,	eRank=eRank_Captain)
OTSBalance=(eTech=eOTS_HP_I,			iCash=600,	eRank=eRank_Colonel)
OTSBalance=(eTech=eOTS_SquadSize_II,	iCash=175,	eRank=eRank_Captain)
OTSBalance=(eTech=eOTS_XP_II,			iCash=250,	eRank=eRank_Captain)
OTSBalance=(eTech=eOTS_HP_II,			iCash=275,	eRank=eRank_Lieutenant)

; Techs
TechBalance=(eTech=eTech_WeaponFragments,	iTime=2,  iAlloys=0,  iElerium=0,  iNumFragments=5,  iNumItems=0)
TechBalance=(eTech=eTech_AlienMaterials,	iTime=2,  iAlloys=0,  iElerium=0,  iNumFragments=5,  iNumItems=0)
TechBalance=(eTech=eTech_Exp_Warfare,		iTime=3,  iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=0)
TechBalance=(eTech=eTech_Xenobiology,		iTime=2,  iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=4)

TechBalance=(eTech=eTech_LaserWeapons,		iTime=6,  iAlloys=5,  iElerium=0,  iNumFragments=5,  iNumItems=0)
TechBalance=(eTech=eTech_PrecisionLasers,	iTime=8,  iAlloys=5,  iElerium=1,  iNumFragments=10,  iNumItems=0)
TechBalance=(eTech=eTech_HeavyLasers,		iTime=8,  iAlloys=15,  iElerium=1,  iNumFragments=15,  iNumItems=0)
TechBalance=(eTech=eTech_Plasma_Pistol,		iTime=20, iAlloys=20,  iElerium=5,  iNumFragments=25, iNumItems=1)
TechBalance=(eTech=eTech_Plasma_Light,		iTime=22, iAlloys=30,  iElerium=7,  iNumFragments=35, iNumItems=1)
TechBalance=(eTech=eTech_Plasma_Rifle,		iTime=40, iAlloys=30, iElerium=10, iNumFragments=45, iNumItems=1)
TechBalance=(eTech=eTech_Plasma_Heavy,		iTime=22, iAlloys=50, iElerium=20, iNumFragments=50, iNumItems=1)
TechBalance=(eTech=eTech_Plasma_Sniper,		iTime=20, iAlloys=60, iElerium=30, iNumFragments=55, iNumItems=0)
TechBalance=(eTech=eTech_Alloy_Cannon,		iTime=20, iAlloys=60, iElerium=35,  iNumFragments=55, iNumItems=0)
TechBalance=(eTech=eTech_BlasterLauncher,	iTime=45, iAlloys=300, iElerium=35, iNumFragments=55, iNumItems=0)

TechBalance=(eTech=eTech_Plasma_Cannon,		iTime=10, iAlloys=50,  iElerium=20, iNumFragments=20, iNumItems=0)
TechBalance=(eTech=eTech_EMP,				iTime=60, iAlloys=175,  iElerium=30, iNumFragments=25,  iNumItems=40)
TechBalance=(eTech=eTech_Fusion_Launcher,	iTime=40, iAlloys=320, iElerium=50, iNumFragments=35, iNumItems=0)

TechBalance=(eTech=eTech_ArcThrower,		iTime=3,  iAlloys=0,  iElerium=0,  iNumFragments=15, iNumItems=0)
TechBalance=(eTech=eTech_BaseShard,			iTime=3,  iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=0)
TechBalance=(eTech=eTech_AlienNavigation,	iTime=5,  iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=2)
TechBalance=(eTech=eTech_UFOPowerSource,	iTime=7,  iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=1)
TechBalance=(eTech=eTech_Firestorm,			iTime=100, iAlloys=200, iElerium=40,  iNumFragments=15, iNumItems=0)
TechBalance=(eTech=eTech_Elerium,			iTime=7,  iAlloys=0,  iElerium=10, iNumFragments=0,  iNumItems=0)
TechBalance=(eTech=eTech_Armor_Carapace,	iTime=7,  iAlloys=10, iElerium=0,  iNumFragments=0,  iNumItems=0)
TechBalance=(eTech=eTech_Armor_Skeleton,	iTime=10, iAlloys=15, iElerium=0,  iNumFragments=0,  iNumItems=0)
TechBalance=(eTech=eTech_Armor_Titan,		iTime=60, iAlloys=60, iElerium=5,  iNumFragments=0,  iNumItems=0)
TechBalance=(eTech=eTech_Armor_Ghost,		iTime=47, iAlloys=15, iElerium=45, iNumFragments=0,  iNumItems=0)
TechBalance=(eTech=eTech_Armor_ArchAngel,	iTime=52, iAlloys=35, iElerium=55, iNumFragments=0,  iNumItems=0)
TechBalance=(eTech=eTech_PsiArmor,			iTime=15, iAlloys=10, iElerium=10, iNumFragments=0,  iNumItems=0)
TechBalance=(eTech=eTech_Hyperwave,			iTime=8,  iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=0)
TechBalance=(eTech=eTech_PsiLink,			iTime=10, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=0)

TechBalance=(eTech=eTech_InterrogateSectoid,			iTime=2, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=0)
TechBalance=(eTech=eTech_InterrogateFloater,			iTime=2, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=0)
TechBalance=(eTech=eTech_InterrogateMuton,				iTime=2, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=0)
TechBalance=(eTech=eTech_InterrogateThinMan,			iTime=2, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=0)
TechBalance=(eTech=eTech_InterrogateEthereal,			iTime=10,iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=0)
TechBalance=(eTech=eTech_InterrogateBerserker,			iTime=2, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=0)
TechBalance=(eTech=eTech_InterrogateMutonElite,			iTime=2, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=0)
TechBalance=(eTech=eTech_InterrogateHeavyFloater,		iTime=2, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=0)
TechBalance=(eTech=eTech_InterrogateSectoidCommander,	iTime=6, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=0)

TechBalance=(eTech=eTech_AutopsySectoid,			iTime=2, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=1)
TechBalance=(eTech=eTech_AutopsyFloater,			iTime=2, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=1)
TechBalance=(eTech=eTech_AutopsyMuton,				iTime=2, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=1)
TechBalance=(eTech=eTech_AutopsyThinMan,			iTime=2, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=1)
TechBalance=(eTech=eTech_AutopsyCryssalid,			iTime=2, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=1)
TechBalance=(eTech=eTech_AutopsyEthereal,			iTime=6, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=1)
TechBalance=(eTech=eTech_AutopsyCyberdisc,			iTime=2, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=1)
TechBalance=(eTech=eTech_AutopsyDrone,				iTime=2, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=1)
TechBalance=(eTech=eTech_AutopsySectopod,			iTime=2, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=1)
TechBalance=(eTech=eTech_AutopsyBerserker,			iTime=2, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=1)
TechBalance=(eTech=eTech_AutopsyMutonElite,			iTime=2, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=1)
TechBalance=(eTech=eTech_AutopsyHeavyFloater,		iTime=2, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=1)
TechBalance=(eTech=eTech_AutopsySectoidCommander,	iTime=3, iAlloys=0,  iElerium=0,  iNumFragments=0,  iNumItems=1)
															

[XComGame.XComPrecomputedPath]
BlasterBombSpeed=500.0f