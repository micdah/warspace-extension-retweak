#define MyAppName "Warspace Extension ReTweak"
#define MyAppVersion "1.75.1"
#define MyAppPublisher "BlackAlpha"
#define MyAppURL "http://xcom.nexusmods.com/mods/18"



[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.) -Odalrick
AppId={{46EA1DA2-162D-4F4C-A7CB-49298708E784}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={reg:HKCU\SOFTWARE\Valve\Steam,SteamPath}\steamapps\common\XCom-Enemy-Unknown
AppendDefaultDirName=no
DirExistsWarning=no
AllowNoIcons=yes
OutputBaseFilename=Warspace Extension v1.75.1 Setup
Compression=lzma
SolidCompression=yes
; If you have an Icon you'd like to use this line is used to point at the location and grab it in.
; I made one; unfortunately I'm a programmer so this is an instance of that unfortunate embarrasment
; known as "programmer art" which has been proven to cause suicides when shown to squirrels. -Odalrick
SetupIconFile=warspace.ico



[Components]
; Describes the parts that can be installed.
Name: "main"; Description: "Warspace Mode"; Types: main; Flags: fixed disablenouninstallwarning;
Name: "main/normal"; Description: "Normal"; Flags: exclusive disablenouninstallwarning;
Name: "main/hardcore"; Description: "Hardcore"; Flags: exclusive disablenouninstallwarning;

Name: "gameversion"; Description: "Game Version"; Types: main; Flags: fixed disablenouninstallwarning;
Name: "gameversion/original"; Description: "Original Game"; Flags: exclusive disablenouninstallwarning;
Name: "gameversion/slingshotdlc"; Description: "Slingshot DLC"; Flags: exclusive disablenouninstallwarning;

Name: "language"; Description: "Game Language"; Types: main; Flags: fixed disablenouninstallwarning;
Name: "language/english"; Description: "English"; Flags: exclusive disablenouninstallwarning;
Name: "language/french"; Description: "French"; Flags: exclusive disablenouninstallwarning;
Name: "language/spanish"; Description: "Spanish"; Flags: exclusive disablenouninstallwarning;
Name: "language/other"; Description: "Other"; Flags: exclusive disablenouninstallwarning;



[Types]
Name: "main"; Description: "Warspace"; Flags: iscustom;



[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"



[Messages]
; This text is shown at the first page of the installer, when the installer is initially started.
WelcomeLabel2=[name/ver]%n%nOnly install this mod onto either:%n- A clean original game installation.%n- An existing Warspace installation version 1.73 or newer.%n%nDo not forget to open the ReadMe file inside the mod archive and read all the installation/startup instructions before continuing this setup!

; This text is shown when the user is asked to choose in which directory the files should be installed.
SelectDirLabel3=Select your XCOM Enemy Unknown game folder.
SelectDirBrowseLabel=The path below could be wrong! The installer does not always locate it automatically.%nCheck the path, make sure it points to your game folder.%n%nClick Next when ready.

; This text is shown when the user must choose what components he wants to install.
SelectComponentsLabel2=Choose whether you want to install the normal or hardcore version of the mod;%nSelect your game version;%nThen select the ingame language you currently have enabled.

; This text is shown when the installer runs into a problem and shows an error message.
FileAbortRetryIgnore=Are you sure you are installing to the game folder? If so, check for missing files. You might want to reinstall/repair the game.%n%nClick Retry to try again, Ignore to skip this file (will break the game), or Abort to cancel installation.
FileAbortRetryIgnore2=Are you sure you are installing to the game folder? If so, check for missing files. You might want to reinstall/repair the game.%n%nClick Retry to try again, Ignore to skip this file (will break the game), or Abort to cancel installation.



; The following code is used when creating backups to check if Warspace is already installed, judging by whether the restore file exists or not. If it doesn't exist, True is returned.
; In short, the line that has this check applied will only be executed if the file that is being checked does not exist.
[Code]
function MyFileCheck(FilePath: String): Boolean;
begin
	if not (FileExists(FilePath)) then
		Result := True;
end;



[Files]
; In this section files are copied over. Note that all files marked with "ignoreversion" will get overwritten without a prompt.

; Restore the EXE backup made by this installer, if it exists. If it doesn't, let's hope the user has an original EXE file...
Source: "{app}\Warspace Files\(WE Backup) XComGame.exe"; DestDir: "{app}\Binaries\Win32"; DestName: XComGame.exe; Flags: external uninsneveruninstall skipifsourcedoesntexist ignoreversion;


; Create backups of common files.
Source: "{app}\Binaries\Win32\XComGame.exe"; DestDir: "{app}\Warspace Files"; DestName: (WE Backup) XComGame.exe; Flags: external ignoreversion;
Source: "{app}\XComGame\CookedPCConsole\XComGame.upk"; DestDir: "{app}\Warspace Files"; DestName: (WE Backup) XComGame.upk; Flags: external skipifsourcedoesntexist onlyifdoesntexist ignoreversion; Check: MyFileCheck(ExpandConstant('{app}\Warspace Files\res_backup.bat'));
Source: "{app}\XComGame\CookedPCConsole\XComStrategyGame.upk"; DestDir: "{app}\Warspace Files"; DestName: (WE Backup) XComStrategyGame.upk; Flags: external skipifsourcedoesntexist onlyifdoesntexist ignoreversion; Check: MyFileCheck(ExpandConstant('{app}\Warspace Files\res_backup.bat'));
Source: "{app}\XComGame\CookedPCConsole\XComGame.upk.uncompressed_size"; DestDir: "{app}\Warspace Files"; DestName: (WE Backup) XComGame.upk.uncompressed_size; Flags: external skipifsourcedoesntexist onlyifdoesntexist ignoreversion; Check: MyFileCheck(ExpandConstant('{app}\Warspace Files\res_backup.bat'));
Source: "{app}\XComGame\CookedPCConsole\XComStrategyGame.upk.uncompressed_size"; DestDir: "{app}\Warspace Files"; DestName: (WE Backup) XComStrategyGame.upk.uncompressed_size; Flags: external skipifsourcedoesntexist onlyifdoesntexist ignoreversion; Check: MyFileCheck(ExpandConstant('{app}\Warspace Files\res_backup.bat'));


; Create backups of Slingshot files.
Source: "{app}\XComGame\DLC\PCConsole\DLC_Day060\Config\XComGame.ini"; DestDir: "{app}\Warspace Files"; DestName: (WE Backup) XComGame.ini; Flags: external skipifsourcedoesntexist onlyifdoesntexist ignoreversion;
Source: "{app}\XComGame\DLC\PCConsole\DLC_Day060\CookedPCConsole\DLC_Day060_SF.upk"; DestDir: "{app}\Warspace Files"; DestName: (WE Backup) DLC_Day060_SF.upk; Flags: external skipifsourcedoesntexist onlyifdoesntexist ignoreversion;


; Backup localization files for English.
Source: "{app}\XComGame\Localization\INT\XComGame.int"; DestDir: "{app}\Warspace Files"; DestName: "(WE Backup Game Folder) XComGame.int"; Flags: external skipifsourcedoesntexist onlyifdoesntexist ignoreversion; Check: MyFileCheck(ExpandConstant('{app}\Warspace Files\res_backup.bat'));
Source: "{userdocs}\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComGame.int"; DestDir: "{app}\Warspace Files"; DestName: "(WE Backup My Docs) XComGame.int"; Flags: external skipifsourcedoesntexist onlyifdoesntexist ignoreversion; Check: MyFileCheck(ExpandConstant('{app}\Warspace Files\res_backup.bat'));
Source: "{userdocs}\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComStrategyGame.int"; DestDir: "{app}\Warspace Files"; DestName: "(WE Backup My Docs) XComStrategyGame.int"; Flags: external skipifsourcedoesntexist onlyifdoesntexist ignoreversion; Check: MyFileCheck(ExpandConstant('{app}\Warspace Files\res_backup.bat'));


; Backup localization files for French.
Source: "{userdocs}\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComGame.fra"; DestDir: "{app}\Warspace Files"; DestName: "(WE Backup My Docs) XComGame.fra"; Flags: external skipifsourcedoesntexist onlyifdoesntexist ignoreversion; Check: MyFileCheck(ExpandConstant('{app}\Warspace Files\res_backup.bat'));
Source: "{userdocs}\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComStrategyGame.fra"; DestDir: "{app}\Warspace Files"; DestName: "(WE Backup My Docs) XComStrategyGame.fra"; Flags: external skipifsourcedoesntexist onlyifdoesntexist ignoreversion; Check: MyFileCheck(ExpandConstant('{app}\Warspace Files\res_backup.bat'));


; Backup localization files for Spanish.
Source: "{userdocs}\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComGame.esn"; DestDir: "{app}\Warspace Files"; DestName: "(WE Backup My Docs) XComGame.esn"; Flags: external skipifsourcedoesntexist onlyifdoesntexist ignoreversion; Check: MyFileCheck(ExpandConstant('{app}\Warspace Files\res_backup.bat'));
Source: "{userdocs}\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComStrategyGame.esn"; DestDir: "{app}\Warspace Files"; DestName: "(WE Backup My Docs) XComStrategyGame.esn"; Flags: external skipifsourcedoesntexist onlyifdoesntexist ignoreversion; Check: MyFileCheck(ExpandConstant('{app}\Warspace Files\res_backup.bat'));


; Temporarily backup "xcom-orginal.exe", if it exists.
Source: "{app}\Binaries\Win32\xcom-orginal.exe"; DestDir: "{app}\Binaries\Win32"; DestName: "(WE Temp) xcom-orginal.exe"; Flags: external skipifsourcedoesntexist deleteafterinstall ignoreversion;



; Install common files.
Source: Common\UPK Files\XComGame.upk; DestDir: "{app}\XComGame\CookedPCConsole"; Flags: uninsneveruninstall ignoreversion;
Source: Common\UPK Files\XComStrategyGame.upk; DestDir: "{app}\XComGame\CookedPCConsole"; Flags: uninsneveruninstall ignoreversion;
Source: Common\Tools\XSHAPE\COPYING; DestDir: "{app}\Warspace Files\XSHAPE"; Flags: ignoreversion;
Source: Common\Tools\XSHAPE\XShape.java; DestDir: "{app}\Warspace Files\XSHAPE"; Flags: ignoreversion;
Source: Common\Tools\XSHAPE\XSHAPE.README.txt; DestDir: "{app}\Warspace Files\XSHAPE"; Flags: ignoreversion;
Source: "Common\Tools\!!!!! DON'T TOUCH THESE FILES !!!!!"; DestDir: "{app}\Warspace Files"; Flags: ignoreversion;
Source: "Common\Guide\Warspace Extension Tips & Tricks.pdf"; DestDir: "{app}"; Flags: ignoreversion;

; Install Slingshot files.
Source: Slingshot\INI Files\XComGame.ini; DestDir: "{app}\XComGame\DLC\PCConsole\DLC_Day060\Config\"; Components: gameversion/slingshotdlc; Flags: uninsneveruninstall ignoreversion;
Source: Slingshot\UPK Files\DLC_Day060_SF.upk; DestDir: "{app}\XComGame\DLC\PCConsole\DLC_Day060\CookedPCConsole\"; Components: gameversion/slingshotdlc; Flags: uninsneveruninstall ignoreversion;


; Install files for Warspace normal mode.
Source: Normal\Warspace_ReadMe.txt; DestDir: "{app}"; Components: main/normal; Flags: ignoreversion;
Source: Normal\INI Files\DefaultGameCore_t.mod; DestDir: "{app}\Binaries\Win32"; Components: main/normal; Flags: deleteafterinstall ignoreversion;

; Install files for Warspace hardcore mode.
Source: Hardcore\WarspaceHM_Notes.txt; DestDir: "{app}"; Components: main/hardcore;  Flags: ignoreversion;
Source: Hardcore\INI Files\DefaultGameCore_hm_t.mod; DestDir: "{app}\Binaries\Win32"; Components: main/hardcore; Flags: deleteafterinstall ignoreversion;


; Install localization files for English + Original Game.
Source: "Localization\INT\(Game Folder Original) XComGame.int"; DestDir: "{app}\XComGame\Localization\INT"; DestName: "XComGame.int"; Components: "gameversion/original and language/english"; Flags: uninsneveruninstall ignoreversion;
Source: "Localization\INT\(My Docs Original) XComStrategyGame.int"; DestDir: "{userdocs}\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT"; DestName: "XComStrategyGame.int"; Components: "gameversion/original and language/english"; Flags: uninsneveruninstall ignoreversion;

; Install localization files for English + Slingshot DLC.
Source: "Localization\INT\(Game Folder Original) XComGame.int"; DestDir: "{app}\XComGame\Localization\INT"; DestName: "XComGame.int"; Components: "gameversion/slingshotdlc and language/english"; Flags: uninsneveruninstall ignoreversion;
Source: "Localization\INT\(My Docs Slingshot) XComGame.int"; DestDir: "{userdocs}\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT"; DestName: "XComGame.int"; Components: "gameversion/slingshotdlc and language/english"; Flags: uninsneveruninstall ignoreversion;
Source: "Localization\INT\(My Docs Slingshot) XComStrategyGame.int"; DestDir: "{userdocs}\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT"; DestName: "XComStrategyGame.int"; Components: "gameversion/slingshotdlc and language/english"; Flags: uninsneveruninstall ignoreversion;

; Install localization files for French + Original Game.
Source: "Localization\FRA\(My Docs Original) XComGame.fra"; DestDir: "{userdocs}\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA"; DestName: "XComGame.fra"; Components: "gameversion/original and language/french"; Flags: uninsneveruninstall ignoreversion;
Source: "Localization\FRA\(My Docs Original) XComStrategyGame.fra"; DestDir: "{userdocs}\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA"; DestName: "XComStrategyGame.fra"; Components: "gameversion/original and language/french"; Flags: uninsneveruninstall ignoreversion;

; Install localization files for French + Slingshot DLC.
Source: "Localization\FRA\(My Docs Slingshot) XComGame.fra"; DestDir: "{userdocs}\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA"; DestName: "XComGame.fra"; Components: "gameversion/slingshotdlc and language/french"; Flags: uninsneveruninstall ignoreversion;
Source: "Localization\FRA\(My Docs Slingshot) XComStrategyGame.fra"; DestDir: "{userdocs}\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA"; DestName: "XComStrategyGame.fra"; Components: "gameversion/slingshotdlc and language/french"; Flags: uninsneveruninstall ignoreversion;


; Install localization files for Spanish + Original Game.
Source: "Localization\ESN\(My Docs Original) XComGame.esn"; DestDir: "{userdocs}\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN"; DestName: "XComGame.esn"; Components: "gameversion/original and language/spanish"; Flags: uninsneveruninstall ignoreversion;
Source: "Localization\ESN\(My Docs Original) XComStrategyGame.esn"; DestDir: "{userdocs}\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN"; DestName: "XComStrategyGame.esn"; Components: "gameversion/original and language/spanish"; Flags: uninsneveruninstall ignoreversion;

; Install localization files for Spanish + Slingshot DLC.
Source: "Localization\ESN\(My Docs Slingshot) XComGame.esn"; DestDir: "{userdocs}\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN"; DestName: "XComGame.esn"; Components: "gameversion/slingshotdlc and language/spanish"; Flags: uninsneveruninstall ignoreversion;
Source: "Localization\ESN\(My Docs Slingshot) XComStrategyGame.esn"; DestDir: "{userdocs}\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN"; DestName: "XComStrategyGame.esn"; Components: "gameversion/slingshotdlc and language/spanish"; Flags: uninsneveruninstall ignoreversion;



; Install some tools/scripts. These are the tools used to install the mod.
; They are all removed at the very end of the installation.
Source: Common\Tools\run_init.bat; DestDir: "{app}"; Flags: deleteafterinstall ignoreversion;
Source: Common\Tools\run_end.bat; DestDir: "{app}"; Flags: deleteafterinstall ignoreversion;
Source: Common\Tools\modpatcher_t.exe; DestDir: "{app}\Binaries\Win32"; Flags: deleteafterinstall ignoreversion;
Source: Common\Tools\XSHAPE_t.config; DestDir: "{app}"; Flags: deleteafterinstall ignoreversion;
Source: Common\Tools\XSHAPE_t.jar; DestDir: "{app}"; Flags: deleteafterinstall ignoreversion;


; Install the restore script. This file is executed by the uninstaller. It restores the backups that were made when the mod was initially installed.
Source: Common\Tools\res_backup.bat; DestDir: "{app}\Warspace Files"; Flags: ignoreversion;



[Run]
; These programs/scripts are run AFTER the files have been copied (see Files section) and BEFORE the installer removes the files marked as "deleteafterinstall".

; This script removes the "uncompressed_size" and "xcom-orginal.exe" files.
Filename: "run_init.bat"; WorkingDir: "{app}"; StatusMsg: "Installing..."; Flags: runhidden;

; This runs the modpatcher. It either uses the normal or hardcore ini file, depending on what the user has chosen at the beginning of the installer.
Filename: "{app}\Binaries\Win32\modpatcher_t.exe"; Parameters: DefaultGameCore_t.mod; StatusMsg: "Installing..."; Components: main/normal; Flags: runhidden;
Filename: "{app}\Binaries\Win32\modpatcher_t.exe"; Parameters: DefaultGameCore_hm_t.mod; StatusMsg: "Installing..."; Components: main/hardcore; Flags: runhidden;

; This runs XSHAPE multiple times, using different ways to call Java. This is a dirty way to make sure it works on different Java installations. On the bright side, no user input is needed.
Filename: "{reg:HKLM64\SOFTWARE\JavaSoft\Java Runtime Environment\1.7,JavaHome}\bin\java.exe"; Parameters: -jar XSHAPE_t.jar 0; WorkingDir: {app}; StatusMsg: "Installing..."; Flags: skipifdoesntexist runhidden; Check: IsWin64;
Filename: "{reg:HKLM32\SOFTWARE\JavaSoft\Java Runtime Environment\1.7,JavaHome}\bin\java.exe"; Parameters: -jar XSHAPE_t.jar 0; WorkingDir: {app}; StatusMsg: "Installing..."; Flags: skipifdoesntexist runhidden;
Filename: "java"; Parameters: -jar XSHAPE_t.jar 0; WorkingDir: {app}; StatusMsg: "Installing..."; Flags: skipifdoesntexist runhidden;

; This script removes the new EXE backup file created by the modpatcher. It then restores the old "xcom-orginal.exe".
Filename: "run_end.bat"; WorkingDir: "{app}"; StatusMsg: "Installing..."; Flags: runhidden;

; Show checkbox to open ReadMe at the end of the setup.
Filename: "{app}\Warspace_ReadMe.txt"; Description: "View the ReadMe."; Flags: shellexec postinstall skipifsilent;
Filename: "{app}\WarspaceHM_Notes.txt";  Description: "View the Hardcore Mode notes."; Components: main/hardcore; Flags: shellexec postinstall skipifsilent;
Filename: "{app}\Warspace Extension Tips & Tricks.pdf"; Description: "View the gameplay guide. (PDF)"; Flags: shellexec postinstall skipifsilent;



[UninstallRun]
; This section dictates what the uninstaller does. Note that files that have NOT been marked with "uninsneveruninstall" will be deleted at the very end.

;This script restores the backups made when the mod was initially installed. After this, the uninstaller will delete files not marked with "uninsneveruninstall".
Filename: "{app}\Warspace Files\res_backup.bat"; StatusMsg: "Uninstalling..."; RunOnceId: "DelService"; Flags: runhidden;