
=====================================================================
=====================================================================
===
=== Warspace Extension
=== version 1.75
===
=== By BlackAlpha
===
=== A mod for XCOM: Enemy Unknown - Game version "356266"
===
=== Supported languages: English, French, Spanish
=== 
=== For the latest version of this mod, please go to XCOM Nexus:
=== http://xcom.nexusmods.com/
===
=====================================================================
=====================================================================

This mod tweaks XCOM to make better use of the game's features and give more control to the player.
I wanted the player's decisions to become more meaningful, rather than being punished randomly by the game all the time.
So now, if you play it right, you will be rewarded. But if you play it wrong, you will be punished.

Note that this mod was designed to be played on the Classic difficulty (with full AI) but Impossible works just fine too.
Don't worry about it being too hard, the difficulty is the same as Normal, just with full AI and a few more enemies.

Also keep in mind that this mod was not designed for save scumming. If you save scum a lot, you might find this mod too easy.
This mod is more "fair" than vanilla and is best played by letting the bad decisions simply happen, don't reload. You can recover from mistakes.


Please remember that, in a sense, this mod is the result of the entire XCOM modding community.
The following people helped the development of this mod directly or indirectly. See the credits for what they've been credited for:
(In no particular order) Drakous79, Johnnylump, Odalrick, Bokauk, Ericfr, Osito2dancer, Perceptiontwist, Yzaxtol, Daemonjax, Dreadylein, Freetup1, Gnarlyrider,
Goemonchan, ResonansER, Ajaxjs, Rookarike, JuggernautOfWar, Svinlesha, Iamalex117, JohnnyZeWolf, Taggart, Derangedftw, Harveysee, Silverfold,
CushVA, Elidar, Rheyah, Eekeek, Ned99, Nolanoth, Tbkiah, Pannory

As you can see, quite a few people contributed to this mod. This mod wouldn't be where it is now if it wasn't for all those people.
If you think you can contribute to this mod somehow, don't hesitate to PM me on XCOM Nexus.



Summary of what this mod does:

The Tactical Combat:
- The tactical game has been rebalanced in such a way that the player's skill becomes more important. It's now less about luck, more about what you decide to do.
- Various aspects of the game have been tweaked to enhance the tactical possibilities and give the player more options.
- For instance, some armors give you increased inventory space, but there are consequences to that. So you need to weigh the pros and cons.
- Another example, the different weapon tiers are useful for different playstyles, making all weapons useful throughout the entire game, depending on how you like to play.
- This increase in tactical possibilities obviously gives the player an advantage, and therefore, the enemy AI has been rebalanced for these new changes.
- The Enemy AI is now completely unshackled, it has more units who move around, and the amount of enemies has been increased for certain missions.
- And the AI no longer relies on cheating stats to win; the stats have been changed to ensure that. Although, high level enemies are still pretty dangerous.
- In short, it all boils down to the player receiving more choices with appropriate consequences, so that the player can make more of an impact through careful consideration.


The Strategic Game:
- The strategic game has been overhauled in such a way that it becomes more of an interactive game, where you do things and you make a difference through your actions.
- There's now a better sense of progression, making it more rewarding to progress on the strategic game. It's now also more important to consider carefully how you want to proceed.
- You need engineers, scientists, labs, workshops, better gear, new aircraft weaponry, this and that, but this all costs time and resources, of which you only have so much.
- For example, early in the game you might need to decide whether you want better armor, weapons, or miscellaneous items, because you might not have enough resources for everything. 
- So basically, you now need to figure out how to balance the progression of your base, while juggling with the UFO and tactical combat aspects. 


The UFO Combat:
- UFO combat now requires a bit more thought. You can't steamroll over the enemy UFOs any longer.
- You must research, gain better aircraft weaponry, buy as many aircraft as possible, and make good use of consumable powerups.
- While starting aircraft weapons can take out smaller UFOs, it's more efficient to gain better weapons as soon as possible.
- And at some point, you will need to get your hands on high tech weaponry if you wish to take out the larger UFOs, which you will need to keep advancing on the strategic game.


Other Things:
- This mod is designed to be played on the Classic difficulty level. You may also try Impossible, it will introduce a higher number of enemies.
- English, French, and Spanish text descriptions have been modified to reflect the changes of this mod. It's not required but it is recommended to play with one of those languages.
- This mod is fully compatible with the Slingshot DLC.
- And much more...



==================================================
===
=== How to INSTALL the mod (Warspace Installer)
===
==================================================

Requirements before you can install the mod:

- Install this mod onto an original game installation, OR, install it onto a Warspace version 1.73 or newer.
To revert to an original game installation, you can uninstall and then reinstall the game, or let Steam verify the files (repair the game).


- The different mod tools used to install this mod expect the game (XCOM: EU) to be installed inside the main Steam directory.
At the time this game came out, you couldn't change the game directory in Steam,
and that's why this mod is not compatible with Steam Libraries that are located outside of the main Steam folder.
In other words, make sure XCOM: EU is installed inside the main Steam folder before you install this mod.


- Java and .NET are required to install this mod. If you are unsure if you got those or if you experience problems, then you should download the latest version of both.
Here are the links:

http://www.java.com/en/download/manual.jsp
http://www.microsoft.com/net


- This mod is only compatible with the following ingame languages: English, French, Spanish.
You can change your ingame language by following the instructions at number 14 of the Warspace technical FAQ:
http://forums.nexusmods.com/index.php?/topic/834174-warspace-technical-support-v150/

I'd like to make this mod compatible with the other officially supported languages, but I need help from other people to do that. PM me on XCOM Nexus if you would like to help out.
You can install this mod onto other languages, but you will be missing out on the changes to the text descriptions.


- Installing any sort of official game update, like an offcial game patch or DLC, on top of a modded game may corrupt your game.
You will then need to repair your game before installing this mod. See number 3 of the technical support FAQ on how to restore/repair your game to the original state:
http://forums.nexusmods.com/index.php?/topic/834174-warspace-technical-support-v150/





Mod Install Instructions:

1. Temporarily disable your anti-virus program.
If your anti virus program blocks the installer while it's installing, it could potentially corrupt your game installation.


2. Run the "Warspace Extension Setup.exe" which can be found inside the mod archive and follow the instructions on the screen.


3. Make sure you point the installer to your root game directory (main game folder).
Example: C:\Program Files (x86)\Steam\steamapps\common\XCom-Enemy-Unknown


4. You can choose between installing the normal or the hardcore version. You most likely want to install the normal version.
Hardcore mode is for people who want an extreme challenge.


5. Select whether you are using the original game or whether there's DLC installed.


6. Then select the language that is currently enabled ingame.


7. Once the installation is complete, please read the following info and then start the game as described in the next section.


GAMEPLAY GUIDE: If you find the gameplay too difficult, then it's recommended to read the gameplay guide. It contains a lot of useful hints.
You may find the guide inside: 1. The mod archive. 2. At the end of the installation setup. 3. Inside the game folder (after the mod has been installed).
The gameplay guide is a PDF file. You can open it using a free program called Adobe Reader: http://get.adobe.com/reader/


TECHNICAL ISSUES: Please go to the following link if you experience technical issues.
http://forums.nexusmods.com/index.php?/topic/834174-warspace-technical-support-v150/


COMPATIBILITY: This mod may or may not be compatible with another mod that modifies one of the following files:
"XComGame.exe", "XComGame.upk", "XComStrategyGame.upk", "DLC_Day060_SF.upk", "DLC_Day060\Config\XComGame.ini", "XComGame.int", "XComStrategyGame.int",
"XComGame.fra", "XComStrategyGame.fra", "XComGame.esn", "XComStrategyGame.esn"
This mod should be compatible with everything else. No promises, though. If you run other mods next to this one, you do so at your own risk!


PIRATED COPIES: Pirated copies are not compatible with the installer of this mod.
Next to that, pirated copies will crash after they install the vanilla files of Warspace.
Go to a pirate website to figure out how to run this mod with your pirated copy.



==================================================
===
=== How to START the mod (Steam Offline Mode)
===
==================================================

You basically need to block "XComGame.exe" from accessing the internet.
There are many ways to do this. Below is described an easy way for regular people:

1. Start Steam.


2. Open the Steam window and click in the top left on: "Steam" > "Go offline" > "Restart in offline mode".


3. Wait until Steam is restarted in offline mode.


4. Before continuing, please read the following info:

CAMPAIGN: It is required to start a new campaign since version 1.70!
If you are new to this mod or are coming from a version older than 1.70, then you need to start a new campaign.
If you've already started a new campaign since 1.70, then you do not need to restart.

TUTORIAL: The tutorial doesn't work, so make sure you disable it when starting a new campaign!

SECOND WAVE: It is not recommended to enable any Second Wave options when you start a new campaign.
Second Wave can make the gameplay unbalanced and affect your experience in a negative way.


5. Now start the game through Steam and have fun!



==================================================
===
=== How to UNINSTALL the mod (Warspace Installer)
===
==================================================

1. Temporarily disable your anti-virus program.
If your anti virus program blocks the uninstaller while it's uninstalling, it could potentially corrupt your game installation.


2. Go to your installed software list in Windows.


3. Choose to uninstall Warspace.


4. After the uninstaller has finished, your game will be restored using the automatic backups made when you initially installed this mod.


5. Done.


Note: If you only install this mod and no other mods, then you should be able to install and uninstall this mod as many times as you want without corrupting the game.

If you do install other mods, then keep in mind the uninstaller will restore some backups, which could potentially undo the changes the other mods bring
or potentially corrupt your game. See the section "Modifications to game files" down below to see which backups are restored.



==================================================
===
=== Technical Issues
===
==================================================

Please look for help at the following link if you experience technical issues:
http://forums.nexusmods.com/index.php?/topic/834174-warspace-technical-support-v150/



==================================================
===
=== Modifications to game files
===
==================================================

The following files are created (they are automatically removed when the mod is uninstalled):

- (Game folder)\Warspace Files\ - This folder contains the backups created during the install process. It also contains some technical files.

- (Game folder)\two text (.txt) files - ReadMe and hardcore mode notes. Hardcore mode notes are only there if hardcore mode is installed.

- (Game folder)\PDF file - The gameplay guide.

- (Game folder)\uninsXXX - These are the files for the uninstaller.



The following backups are made when the mod is installed for the first time only (they are automatically restored when the mod is uninstalled):

- (game folder)\Binaries\Win32\XComGame.exe

- (Game folder)\XComGame\CookedPCConsole\XcomGame.upk

- (Game folder)\XComGame\CookedPCConsole\XComStrategyGame.upk

- (Game folder)\XComGame\DLC\PCConsole\DLC_Day060\CookedPCConsole\DLC_Day060_SF.upk

- (Game folder)\XComGame\CookedPCConsole\XcomGame.upk.uncompressed_size

- (Game folder)\XComGame\CookedPCConsole\XComStrategyGame.upk.uncompressed_size

- (Game folder)\XComGame\DLC\PCConsole\DLC_Day060\Config\XComGame.ini

- (Game folder)\XComGame\Localization\INT\XComGame.int

- (My Documents)\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComStrategyGame.int

- (My Documents)\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComGame.int

- (My Documents)\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComStrategyGame.fra

- (My Documents)\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComGame.fra

- (My Documents)\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComStrategyGame.esn

- (My Documents)\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComGame.esn



The following files are overwritten or created if they didn't exist yet (they are automatically restored (or removed if no backups exist) when the mod is uninstalled):

- (game folder)\Binaries\Win32\XComGame.exe

- (Game folder)\XComGame\CookedPCConsole\XcomGame.upk

- (Game folder)\XComGame\CookedPCConsole\XComStrategyGame.upk

- (Game folder)\XComGame\DLC\PCConsole\DLC_Day060\CookedPCConsole\DLC_Day060_SF.upk  (When mod is installed with option: Slingshot)

- (Game folder)\XComGame\DLC\PCConsole\DLC_Day060\Config\XComGame.ini  (When mod is installed with option: Slingshot)

- (Game folder)\XComGame\Localization\INT\XComGame.int  (When mod is installed with options: English, Original)

- (My Documents)\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComStrategyGame.int  (When mod is installed with options: English, Original, Slingshot)

- (My Documents)\My Games\XCOM - Enemy Unknown\XComGame\Localization\INT\XComGame.int  (When mod is installed with options: English, Slingshot)

- (My Documents)\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComStrategyGame.fra  (When mod is installed with options: French, Original, Slingshot)

- (My Documents)\My Games\XCOM - Enemy Unknown\XComGame\Localization\FRA\XComGame.fra  (When mod is installed with options: French, Original, Slingshot)

- (My Documents)\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComStrategyGame.esn  (When mod is installed with options: Spanish, Original, Slingshot)

- (My Documents)\My Games\XCOM - Enemy Unknown\XComGame\Localization\ESN\XComGame.esn  (When mod is installed with options: Spanish, Original, Slingshot)



The following files are removed (they are automatically restored when the mod is uninstalled):

- (Game folder)\XComGame\CookedPCConsole\XcomGame.upk.uncompressed_size

- (Game folder)\XComGame\CookedPCConsole\XComStrategyGame.upk.uncompressed_size



If you are a modder and want to know what parts of the code I've changed, go here:
http://forums.nexusmods.com/index.php?/topic/808571-warspace-extension-discussion/



==================================================
===
=== Included Software
===
==================================================

This mod contains the following software:

"Modpatcher" version 0.12, created by Dreadylein. This program can modify certain gameplay related INI settings inside the EXE file.
More info:  http://forums.nexusmods.com/index.php?/topic/804882-modpatcher/

"XSHAPE" version 0.13b, created by Daemonjax. This program allows the game to load modified, uncompressed UPK files.
More info:  http://forums.nexusmods.com/index.php?/topic/810950-xshape/

"Inno Setup 5", created by JRSoftware.org. This is an installer setup program.
More info:  http://www.jrsoftware.org/isinfo.php



==================================================
===
=== Credits
===
==================================================

Mod created by BlackAlpha.


Contributors:
Ericfr (XCOM Nexus) - He did the entire French translation of Warspace!
Osito2dancer (XCOM Nexus) - He did the entire Spanish translation of Warspace!
Perceptiontwist (XCOM Nexus) - He wrote most of the gameplay guide.
Odalrick (XCOM Nexus) - Helped with setting up the installation program.
Drakous79 (XCOM Nexus) - Found out how to disable friendly fire completely; how to enable abductions in countries with a satellite; how to reduce the size of storms.
Johnnylump (XCOM Nexus) - Found out how to change UFO missions; how to increase amount of UFOs; how to increase amount of enemies during abduction missions.
Bokauk (XCOM Nexus) - Found out how to decrease will penalty and how to keep dead bodies from disappearing.
Nolanoth (XCOM Nexus) - Found out how to modify the skill trees.
Daemonjax and Tbkiah (XCOM Nexus) - Found out out how to implement production times.


Very big thanks to the following people for helping with the testing of this mod and providing feedback:
Perceptiontwist, Drakous79, Odalrick, Freetup1, Gnarlyrider, Goemonchan, ResonansER, Ajaxjs, Rookarike, JuggernautOfWar, Svinlesha, Iamalex117, JohnnyZeWolf, Pannory, Osito2dancer,
Taggart, Derangedftw, Harveysee, Silverfold, CushVA, Yzaxtol, Elidar, Rheyah, Eekeek, Ned99


Special thanks to:
The XCOM Nexus community for providing valuable feedback.

Dreadylein (XCOM Nexus) for creating the mod patcher, which helped the modding community to grow.

Daemonjax (XCOM Nexus) for creating XSHAPE, allowing us to modify the UPK files.

JRSoftware.org for creating Inno Setup 5, a pretty cool installer.

Firaxis Games for creating XCOM: Enemy Unknown.



==================================================
===
=== Permission / Legal
===
==================================================

If you want to use this mod inside another mod, or want to redistribute it, or want to use it in some other way,
then you need to ask for my permission first!

You may contact me (BlackAlpha) by a private message on XCOM Nexus:
http://xcom.nexusmods.com/



==================================================
===
=== Changelog
===
==================================================

Version 1.75

- Added a gameplay guide. It can be found in: 1. The mod archive. 2. At the end of the install setup. 3. Inside the game folder (after the mod has been installed).


Version 1.74

- Slightly increased HP of all armors.
- Reduced HP of soldiers and added that HP to armors. (New campaign required to apply this change.)
- Soldiers will only become gravely injured when losing more than half of their HP (like in vanilla).
- Increased HP of Ghost, Psi and starting armors.
- Increased defense bonus of light armors.
- Reduced price of Carapace and Skeleton armors.
- Slightly reduced accuracy and mobility of Sectoid, Floater and Thin Man.


Version 1.73

- Mod made compatible with the new game patch.
- Made Small and large Scouts easier to shoot down.


Version 1.72

- If you think the game is too hard, then you should start a new campaign with this update.
- Significantly reduced repair times of Interceptors.
- Increased alloy gain from scout UFOs.
- Reduced costs of Foundry upgrades.
- Interceptor transfer time has been reduced to 5 hours.
- Increased accuracy of soldiers (this won't affect existing soldiers).
- Some minor tweaks.


Version 1.71

- Significantly reduced accuracy of Sectoids.
- Made low level enemies very slightly easier to hit.


Version 1.70

- Starting a new campaign is required!

- The strategic game has been overhauled.
- The strategic game should now make more sense and hence should be more fun. Also, many small tweaks have been made to give it a better sense of progression.
- UFO size and Interceptor weaponry are now correlated in a more intuitive and linear fashion, making it easier to figure out what you can and cannot shoot down with what weaponry.
- Changed UFO missions.
- Increased amount of random UFOs.
- Changed prices/times/requirements of almost all items/research/upgrades/etc.
- Reduced elerium/alloy reward for completing Slingshot.

- Reduced Zhang's HP.
- Reduced enemy numbers during the first mission.
- Increased enemy numbers during abduction, small scout, abductor, supply ship and battleship missions.
- Removed amount limit for certain enemy types.
- Buffed HP of armors and HP gain of soldiers a tiny bit.

- Many small tweaks.


Version 1.63

- Reduced HP of Battleships. This should make it a bit easier to shoot them down.
- Increased XP required on soldiers for higher levels.


Version 1.62

- Fixed continent bonus text (when starting a new campaign) for English Slingshot users.


Version 1.61

- Mod made compatible with new game patch version "347752".
- Due to the game patch, you most likely will need to restore your game to the original before installing this mod. See number 3 of the tech support FAQ.
- Decreased the effect of Marathon mode to make it more compatible with the new mod changes.


Version 1.60

- The installer now shouldn't give 32bit users an error message about 64bit.
- When selecting the install path manually, it now points to the right folder.
- Fixed bug where the installer would create a backup of a modded text file, restoring the modded file when the mod is uninstalled.
- Added support for French and Spanish languages. You can choose your desired language during the installation setup.
- Some other minor tweaks to the installer.
- Fixed minor typos in the difficulty menu.
- Modified aircraft weaponry text descriptions to reflect the changes of this mod.
- Updated "things you need to know" document with new info about overhauled UFO combat. It's recommended to read it.

- Overhauled the strategic game. The strategic game should now be more challenging and the player can influence the game progress a lot more.
- Rebalanced all aircraft and aircraft weapons stats.
- Large Scouts and Abductors can only be shot down with alien aircraft weaponry, so that abductions keep happening until the late game.
- Ignoring a flying UFO will no longer increase the panic rating. It also won't affect the council grading and Bradford won't complain about it.
- During the beginning of the strategic game, small scouts will take the role of satellite hunters.
- Increased repair time of Interceptors.
- Decreased price of aircraft weapons and powerups.
- Increased research costs of high tier aircraft weapons, Firestorm and Blaster Launcher.
- Reduced price of satellites.
- Rebalanced country income levels to make South America and Africa more feasible.
- Decreased continent bonuses.
- Reduced money reward for abduction missions.
- Increased scientist reward for abduction missions.
- Lowered the high amount of money you receive for "fetch quests". The reward amount was bugged and should now be a bit closer to vanilla levels.

- Slightly increased accuracy and HP of high level enemies.
- Slightly increased mobility of Heavy Floaters.
- Slightly lowered HP bonus of high tier armors.
- Lowered HP gain of soldiers.
- Soldiers will now become gravely injured when losing half of their base HP.
- SHIV price slightly reduced.

- Many minor tweaks.


Version 1.53

- The mod should now be fully compatible with the Slingshot DLC. You can now choose during the setup to install the DLC compatible version.
- Simplified the installer. No more need to select the Java version.
- Fixed an issue where the uninstaller wouldn't restore some text files if the user had blocked the EXE from accessing the internet.
- Fixed the modded text descriptions for users with the Slingshot DLC.
- Slightly reduced the size of storms over countries that have left the council.
- The player no longer needs to wait after activating Overwatch or Hunker Down. This allows the player to queue up commands much faster. It makes the gameplay much smoother.


Version 1.52

- Mod made compatible with new game patch.
- Abductions will now happen in countries that have a satellite. A UFO will sometimes try to land there prior to the mission.
- To compensate for the increased UFO activity, the chance of detecting a UFO has been decreased.
- You no longer receive a big discount bonus with just a few engineers. You now need more engineers to receive a decent discount.
- At the monthly report screen, the text "Monthly Reward" has been changed to "Income/Rewards" to make it more clear what kind of info is being shown.
- Increased plasma rifle research time.
- Very slightly increased alien grenade throwing range.
- Increased Floater movement range.
- Decreased Thin Man movement range.


Version 1.51

- Fixed a few typos in the modified text descriptions.
- Updated the text files to the latest official version.
- Fixed bug with uninstaller. It didn't always restore the backup of the localization file in My Documents.


Version 1.50

- Switched to a new installation process that is user friendly. Thanks to Odalrick for helping with that!

- Dead bodies won't disappear (most of the time).
- The will penalty for becoming critically injured has been drastically reduced.
- Completely disabled friendly fire when soldiers panic.
- Switched around 2 abilities (Flush and Holo Targeting) between the Heavy and Support.

- Slightly increased the chance to fire when soldiers panic.
- Slightly increased the amount of XP needed to level up.
- Rebalanced accuracy stats of all classes. It mostly affects the higher levels.
- Slightly reduced defense and movement bonus of light armors.
- Increased price of some high level gear (mostly plasma weapons).
- Slightly rebalanced price of OTS upgrades.

- Changed some text descriptions, so that the descriptions reflect the changes of this mod.
- Implemented new soldier ranks. (Rookie, squaddie... That just didn't sound right to me.)
- Soldiers will now mostly be called "operatives".
- These changes were implemented to make the "secret international organization feeling" more authentic.


Version 1.42

- Mod made compatible with new game patch (patch 2).
- Increased critical hit chance for melee enemies.
- Reduced recovery time a bit.
- Reduced accuracy penalty and damage output of light plasma rifle.


Version 1.41

- Added an alternative Patcher.bat in "(Game folder)\Binaries\Win32" for Windows 8 users to solve the "Files not found" error.
- People who get this error need to run Patcher.bat before running "Warspace Mod Install.bat".


Version 1.40

- Again slightly increased research times to make the lab more useful.
- Labs can be build from the start to give the player more choice in the beginning of the campaign.
- Items now take time to build.
- Winning an abduction mission will now decrease panic by 2 in the country instead of 3.

- SHIV laser now doesn't need to reload.
- Rebalanced SHIV weapons.
- Lowered costs of the SHIVs.
- PSI armor has slightly higher Will bonus.
- Gave soldiers +1 HP. Gave armors -1 HP to increase chances of becoming wounded. It was a bit too easy to avoid it.

- Slightly increased damage of enemies.
- Slightly increased critical hit chance of enemies.

- Adjusted code for calculating recovery times. Gravely injured soldiers will have much higher recovery times, most of the time.
- Slightly increased chances to become critically wounded instead of dying. This was done to make medkits more important.
- Overhauled skill trees of all classes.
- Soldiers who panic will now shoot again. The chance to shoot friendlies has been lowered a lot, but I can't eliminate it entirely yet (still looking).
- Slightly increased chance for AI not to move but instead shoot when their accuracy is low. This should make them shoot at long range a bit more often.
- Items aren't build instantly anymore. It takes a few days to create something.


Version 1.34

- Newly recruited rookies will start with the right amount of HP.


Version 1.33

- Hotfix for plasma research. Fixed short research time of plasma weapons.


Version 1.32

- Slightly increased research time and also increased research lab effectiveness to make the lab more useful.
- Increased research time of plasma weapons by about 25% to make it harder to get them and make lasers a bit more attractive.
- Balanced out the starting stats for the Second Wave option Not Created Equally.
- Slightly rebalanced the sniper class' accuracy at the highest levels.


Version 1.31

- Hotfix for Titan and Archangel armors. They should give you item slots now.


Version 1.3

- Armors rebalanced a bit.
- Armors have a Will bonus now. Think confidence boost based on how well protected the soldier is. The higher the HP bonus, the higher the Will bonus.
- Carapace, Archangel and Ghost armors have an average Will boost. Titan has a very large Will boost. Skeleton has very little.
- Increased Will bonus on the PSI armor to balance out the new Will bonuses on the other armors.
- Most armors provide a bit less HP to promote the use of secondary armors (the ones that go in your item slot).
- Reduced defense on Ghost armor. It simply had too much defense to be considered balanced with these new enemy stats.
- Secondary armors (that go in the item slot) provide more HP.
- Slightly decreased HP on heavy armors to balance out the improved secondary armors and promote the use of them.
- Heavy armors (with two item slots) reduce mobility slightly. All light armors increase mobility and defense.

- Minor accuracy tweaks on some high level enemies. Most enemies received a small accuracy increase.
- Rebalanced enemy defense bonuses. The bigger the enemy, the less defense they have. Smaller enemies have the most defense, which makes them a bit harder to hit.
- Slightly increased HP of all enemies to compensate for the increased weapon damage of conventional weapons (since a few versions ago).

- Rebalanced soldier's Will gain per level due to the armor overhaul.
- Rebalanced accuracy gain per level.
- Reduced soldier's starting HP and moved those hitpoints to the armors to reduce the chances of becoming wounded and needing to recover.

- Lasers now do damage through criticals. They have a low chance to fail a critical and do little damage.
- Lasers don't need to reload.
- Frag grenades do 1 extra damage to compensate for the increased HP on the enemies.

- Panic level 5 countries with a satellite will have a slightly bigger chance to leave the council.
- Panic level 4 countries with no satellite in the continent will have a bit smaller chance to leave the council.
- Lowered the chance of satellites reducing panic in countries with a low panic level.
- The Research Lab's effectiveness has been increased. It's very effective at decreasing research times.
- Research time has been slightly increased to give the Research Lab a useful purpose.
- Facility maintenance costs have been increased, so that you won't be swimming in cash in the late game.
- Workshops are enabled from the start, allowing the player to begin the strategic game with a new starting move.
- Interceptor transfer time to a new base has been reduced from 3 to 1 day.
- Slightly reduced chance of UFOs showing up. Too many UFOs showed up in the late game and it got pretty repetitive.
- Slightly increased chance of UFOs finding stealth satellites.


Version 1.26

- Fixed Marathon mode. It's now possible to build the Hyperwave Relay with just one Hyperwave Beacon.


Version 1.25

- Changed the way the Arc Thrower is used during battles. You no longer need to select it before using the ability. This way a few bugs are fixed.


Version 1.2

- Carapace, Titan and Archangel armors allow for two small item slots, enabling new playstyles. Other armors already are pretty damn good.
- Reduced HP bonus of Archangel armor to 6, to balance out the extra item slot.


Version 1.1

- Low level enemies got an accuracy boost to make the first half of the game a bit harder.
- Plasma weapons got a tiny environment damage boost.
- Changed settings of the Not Created Equally SW option. Rookies won't start with insanely high stats.


Version 1.0

- First release.




